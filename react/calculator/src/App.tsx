import React, { useEffect, useState } from 'react';
import './App.css';
import Display from './components/Display';
import Numbers from './components/Numbers';
import Options from './components/Options';

function App() {

  const [andres, setAndres] = useState<string>('');
  const [total, setTotal] = useState<string>('0');
  const [memoryNumber, setMemoryNumber] = useState<string>('0');

  // For example purpose only
  useEffect(() => {
    if(memoryNumber==='0'){
      document.title = `Add number`;
    }else{
      document.title = `React App`;
    }
  }, [memoryNumber]);

  // For example purpose only
  useEffect(() => {
      document.title = `React App`;
  },[]);

  return (
    <div className="App">
      <div className='panel'>
        <Display memoryNumber={memoryNumber} />
          <div className='keyboard'>
            <Numbers memoryNumber={memoryNumber} setMemoryNumber={setMemoryNumber} />
            <Options memoryNumber={memoryNumber} setMemoryNumber={setMemoryNumber} total={total} setTotal={setTotal} />
          </div>
      </div>
    </div>
  );
}

export default App;
