import React, { useState } from 'react';

interface Props{
  memoryNumber:string;
  setMemoryNumber:React.Dispatch<React.SetStateAction<string>>;
}

const Numbers:React.FC<Props>  = ({memoryNumber, setMemoryNumber}) => {
  let numbers:Array<any> = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0];

  const appendNumber = (number: string) => {
    memoryNumber = (memoryNumber == '0') ? '' : memoryNumber;
    let newNumber = memoryNumber + number;
    setMemoryNumber(newNumber);
  };


  return (
    <div className='numbers'>
      {numbers.map((number) => (
        <button
          key={number}
          onClick={()=>appendNumber(number)}
        >{number}</button>
      ))}
    </div>
  );
}

export default Numbers;