import React, { useState } from 'react';

interface Props{
  memoryNumber:string;
  setMemoryNumber:React.Dispatch<React.SetStateAction<string>>;
  total:string;
  setTotal:React.Dispatch<React.SetStateAction<string>>;
}

const Options:React.FC<Props> = ({memoryNumber, setMemoryNumber, total, setTotal}) => {

  const appendOperation = (operation:string) =>{    
     if(operation == '=' || (operation == '+' && memoryNumber == '0')){
      let finalTotal = parseInt(memoryNumber) + parseInt(total);
      setMemoryNumber(''+ finalTotal);
    }else if(operation == '+' && (memoryNumber.length > 0 && memoryNumber != '0')){
      setTotal(memoryNumber);
      setMemoryNumber('0');
    }
  };

  return (
    <div className='operations'>
      <button onClick={()=>appendOperation('+')}>+</button>
      <button disabled>-</button>
      <button disabled>x</button>
      <button disabled>/</button>
      <button onClick={()=>appendOperation('=')}>=</button>
    </div>
  );
}

export default Options;