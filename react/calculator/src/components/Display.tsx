import React from 'react';

interface Props{
  memoryNumber:string;
}

const Display:React.FC<Props> = ({memoryNumber}) => {
  return (
    <div className='display'>
        {memoryNumber}
    </div>
  );
}

export default Display;