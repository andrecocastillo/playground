"use client"
import Layout from "@/theme/layout";
import UserForm from "../_componets/userForm";
import RestMockup from "@/libs/restmockup";
import { SAVE_USER } from "@/mockups/saveuser";
import { useRouter } from 'next/navigation';

export default function UsersCreate() {
  const router = useRouter();

  
  const saveData = (event) => {
    event.preventDefault();
    console.log('Saving data');
    let restMockup = new RestMockup();
    restMockup.post('/users-create', SAVE_USER)
    .then((data)=>{
      console.log('DATA SAVED!!');
      console.log(data);
      
      router.push('/');
    })
    .catch(()=>{
    })
  };
  
  return (
    <main>
        <Layout>
          <div>Create User</div>
          <UserForm saveData={saveData}/>
        </Layout>
    </main>
  )
}