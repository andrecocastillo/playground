import Layout from "@/theme/layout";
import UserForm from "../_componets/userForm";


export default function UsersCreate() {
  return (
    <main>
        <Layout>
          <div>Update User</div>
          <UserForm />
        </Layout>
    </main>
  )
}
