import Footer from "./footer";
import Header from "./header";
import Menu from "./menu";

export default function Layout(props:any) {
  return (
    <>
      <Header></Header>
      <Menu></Menu>
      <div>
          {props.children}
      </div>
      <Footer></Footer>
    </>
  )
}
