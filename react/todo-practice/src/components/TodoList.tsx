import { Todo } from '../model';
import SingleTodo from './SingleTodo';
import './styles.css';
import { Droppable } from "react-beautiful-dnd";

interface Props{
    todos: Todo[];
    setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
    completedTodos: Todo[];
    setCompletedTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const TodoList:React.FC<Props> = ({todos, setTodos, completedTodos, setCompletedTodos}) => {

    return(

        <div className='container'>
                <Droppable droppableId="TodoList">
                    {(provided, snapshot)=>(
                        <div 
                            className={`todos ${snapshot.isDraggingOver ? "dragactive" : ""}`}
                            ref={provided.innerRef} 
                            {...provided.droppableProps}
                        >
                        <div className='todos__heading'>
                            Active Task
                        </div>
                        {todos.map((t , index) => (
                            <SingleTodo index={index} todo={t} todos={todos} key={t.id} setTodos={setTodos} />
                        ))}
                        {provided.placeholder}
                        </div>
                    )}
                </Droppable>
                <Droppable droppableId="TodoRemove">
                    {(provided, snapshot)=>(
                        <div 
                            className={`todos remove ${snapshot.isDraggingOver ? "dragremove" : ""}`} 
                            ref={provided.innerRef} 
                            {...provided.droppableProps}
                        >
                        <div className='todos__heading'>
                            Completed Task
                        </div>
                        {completedTodos.map((t, index) => (
                            <SingleTodo index={index} todo={t} todos={completedTodos} key={t.id} setTodos={setCompletedTodos} />
                        ))}
                        {provided.placeholder}
                        </div>
                    )}
                </Droppable>

        </div>
    );
}

export default TodoList;