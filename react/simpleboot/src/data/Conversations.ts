export const conversations = [
    [
        {messageId:1, person: 2, message: 'Hello, whats your name'},
        {messageId:2, person: 1, message: 'My name is Andres. And what is your name?'},
        {messageId:3, person: 2, message: 'My name is C3PO'},
        {messageId:4, person: 1, message: 'Good!'}
    ],
    [
        {messageId:5, person: 2, message: 'Hi, all rigth?'},
        {messageId:6, person: 1, message: 'No, I have a complain. I have finished a meeting.'},
        {messageId:7, person: 2, message: 'Really.  Whats up?'},
        {messageId:8, person: 1, message: 'The client PETS SHOPS didn\'t like the presentation about the new web site. I have been exposed to all ideas and this is frustrating because I have been creating this presentation for 3 days besides is very important start with this project as soon as possible.'},
        {messageId:9, person: 2, message: 'Bad, very bad!. And the meeting har already started by the time I arrived.'},
        {messageId:10, person: 1, message: 'Yes, I have just spoken with Ana about creating a brand-new presentation. She had arrived the meeting when the client already was angry.'},
        {messageId:11, person: 2, message: 'Good idea. I need have this client very happy.'},
        {messageId:11, person: 1, message: 'Ok, relax. I am planning to change some ideas.'},
        {messageId:11, person: 2, message: 'I remember my first client. He was annoying. However, I was very fast at creating new concepts for his company while I made the meeting and his mind had been changing since this meeting.'},
        {messageId:11, person: 1, message: 'Good, for the next meeting I will try to get more information about his company.'},
        {messageId:11, person: 2, message: 'Yes, this is a very clever trick.'},

    ]
];
