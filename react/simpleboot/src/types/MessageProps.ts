export type MessageProps = {
    messageId: number
    person: number
    message: string
};