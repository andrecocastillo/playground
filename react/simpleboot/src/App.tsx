import { useEffect } from "react";
import { Routes, Route } from "react-router-dom"
import './App.css';
import { Chat } from "./pages/chat/Chat";

function App() {
  return (
      <Routes>
        <Route path="/" element={<Chat />} />
      </Routes>
  );
}

export default App;