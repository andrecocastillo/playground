
type HeaderProps = {
    useWritingUser:boolean
}

export function Header({useWritingUser}:HeaderProps) {

    console.log(useWritingUser);
    return (
        <div id="header-chat">
            HEAD
            <div style={!useWritingUser ? {} : { display: 'none' }}>
                C3p0 typing...
            </div>
        </div>
    )
}