import React, { useEffect, useState } from 'react';
import { parse } from "path";
import { MessageProps } from "../../types/MessageProps";
import { Message } from "./Message";
import { conversations } from '../../data/Conversations';

type PanelMessagesProps = {
    currentChat:MessageProps[]
}

export function PanelMessages({currentChat}:PanelMessagesProps) {
    return (
        <div id="panel-messages">
            {
                currentChat.map((messageInfo, index) => (
                    <Message key={index} {...messageInfo} />
                ))
            }
        </div>
    )
}