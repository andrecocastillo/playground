import { MessageProps } from "../../types/MessageProps";


export function Message({messageId, person, message}:MessageProps) {

    // 
    return (
        <div className={`message ${person === 1 ? "messaje-user" : ""}`} >
            {message}
        </div>
    )
}