type SendMessageProps = {
    turnUser:boolean
    sendMessage:React.MouseEventHandler<HTMLButtonElement>
    openPaneResponses:React.MouseEventHandler<HTMLElement>
}

export function SendMessage({turnUser, sendMessage, openPaneResponses}:SendMessageProps) {

    return (
        <div>
            <div id="send-message">
                <div id="input-fake" onClick={openPaneResponses}></div><button> S </button>
            </div>
            <div id="panel-responses" className={`${turnUser ? "responses-open" : ""}`}>
                <ul>
                    <li> <input type="radio" value="Male" name="gender" />This is my selected response</li>
                </ul>
                <button onClick={sendMessage}>
                    SEND
                </button>
            </div>
        </div>
    )
}