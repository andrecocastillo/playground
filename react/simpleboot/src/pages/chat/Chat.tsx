import React, { useEffect, useState } from 'react';
import { Header } from "../../components/chat/Header";
import { PanelMessages } from "../../components/chat/PanelMessages";
import { SendMessage } from "../../components/chat/SendMessage";
import { conversations } from '../../data/Conversations';
import { MessageProps } from '../../types/MessageProps';

export function Chat() {
    const [turnUser, setTurnUser] = useState<boolean>(false);
    const [currentChat, setCurrentChat] = useState<MessageProps[]>([]); 
    const [currentNumberMeesage, setCurrentNumberMeesage] = useState<number>(0); 
    const [useWritingUser, setUseWritingUser] = useState<boolean>(false); 
    const conversation = conversations[0];
    
    const appendMessage = () => {
        if(!turnUser){
            setUseWritingUser(false);
            let timer = setTimeout(() => {
                console.log("AAA");

                let increment = currentNumberMeesage;
                let message = conversation[increment];
    
                if(!message) return false;
    
                if(message.person === 2){
                    setCurrentChat([...currentChat, message]);
                    setTurnUser(false);
                    increment++;
                    setCurrentNumberMeesage(increment);
                    setUseWritingUser(true);
                }

            }, 2000);

            // clearTimeout(timer);
        }
    }

    const sendMessage = () => {
        if(turnUser){
            let increment = currentNumberMeesage;
            let message = conversation[increment];

            if(!message){setTurnUser(false); return false;}
            setCurrentChat([...currentChat, message]);
            
            increment++;
            setCurrentNumberMeesage(increment);       

            setTurnUser(false);
        }
    }

    const openPaneResponses = () => {
        setTurnUser(true);
    }

    useEffect(() => {
            appendMessage();
    }, [currentChat]);

    return (
        <div id="general-container">
            <Header useWritingUser={useWritingUser}/>
            <PanelMessages currentChat={currentChat}/>
            <SendMessage turnUser={turnUser} sendMessage={sendMessage} openPaneResponses={openPaneResponses}/>
        </div>
    )
}