import {Row, Col} from 'react-bootstrap'
import StoreItem from '../componets/StoreItem';
import storeItem from '../data/items.json'

function Store() {
  return (
    <div>
        <Row md={2} xs={1} lg={3} className="g-3">
          {storeItem.map(item =>(
            <Col>
              <StoreItem  {...item}/>
            </Col>
          ))}
        </Row>
    </div>
  );
}

export default Store;
