import React from 'react';
import { Container } from 'react-bootstrap';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import Navbar from './componets/Navbar';
import { ShoppingCartProvider } from './context/SoppingCartContext';
import About from './pages/About';
import Home from './pages/Home';
import Store from './pages/Store';

function App() {
  console.log("AAAAA");
  
  return (
    <ShoppingCartProvider>
      <Navbar />
      <Container className='mb-4'>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/about' element={<About />} />
            <Route path='/store' element={<Store />} />
          </Routes>
      </Container>
    </ShoppingCartProvider>
  );
}

export default App;
