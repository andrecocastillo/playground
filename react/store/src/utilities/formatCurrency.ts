const CURRENCY_FORMARTTER = new Intl.NumberFormat(undefined, {currency:"USD", style:"currency"});

export function formatCurrency(number:number){
    return CURRENCY_FORMARTTER.format(number);
}