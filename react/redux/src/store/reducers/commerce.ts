import { createSlice } from "@reduxjs/toolkit";

export const commerceSlice = createSlice({
    name: "commerce",
    initialState:{
        hello:'hello world',
        hello2:'hello world II',
        products: [
            {name: "Producto AAAA"}
        ],
        costo: 100
    },
    reducers:{
        append: (state) =>{
         // state.products;
        },
        increment: (state)=>{
            console.log('>> increment');
            state.costo = state.costo + 5;
        }
    }
});

export const {append, increment} = commerceSlice.actions;

export default commerceSlice.reducer;