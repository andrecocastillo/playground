import { configureStore } from '@reduxjs/toolkit'
import commerceSlice from './reducers/commerce';

const store = configureStore({ 
    reducer: {
        commerce: commerceSlice
    } 
})

export default store;