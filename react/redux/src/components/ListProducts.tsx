import {useDispatch} from "react-redux";
import { increment } from "../store/reducers/commerce";

export function ListProducts() {
    const dispatch = useDispatch();

    const addProduct = () =>{
        console.log(' addProduct ');
        dispatch(increment());
    };

    return (
        <div>
            <div>
                <h3>Product 1</h3>
                <button onClick={addProduct}> ADD </button>
            </div>
        </div>
    )
}