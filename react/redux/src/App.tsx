import React from 'react';
import './App.css';
import { ListProducts } from './components/ListProducts';
import { NavSite } from './components/NavSite';

function App() {
  return (
    <div className="App">
      <NavSite />
      <ListProducts />
    </div>
  );
}

export default App;
