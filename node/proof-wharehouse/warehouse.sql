-- Adminer 4.8.1 MySQL 5.5.5-10.4.24-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `nit` bigint(20) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `state` enum('A','I') NOT NULL DEFAULT 'A',
  `created` datetime NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `companies` (`company_id`, `name`, `address`, `nit`, `phone`, `state`, `created`) VALUES
(20,	'xxxxx',	'carrera 13a #11-14 sur',	12121212,	12121212,	'A',	'2023-02-11 17:24:35'),
(21,	'Maleja',	'carrera 13a #11-14 sur, Bogota',	121212,	13131313,	'A',	'2023-02-11 17:36:32'),
(22,	'xxxxx',	'carrera 13a #11-14 sur',	12121212,	12121212,	'A',	'2023-02-11 17:36:36'),
(23,	'',	'',	0,	0,	'I',	'0000-00-00 00:00:00'),
(24,	'',	'',	0,	0,	'I',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `state` enum('A','I') NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `products` (`product_id`, `company_id`, `name`, `quantity`, `state`, `created`) VALUES
(1,	21,	'The simson name',	2,	'I',	'2023-02-11 19:58:58'),
(2,	21,	'The simson name2',	2,	'I',	'2023-02-11 19:59:15'),
(3,	21,	'The simson name II',	2,	'A',	'2023-02-11 19:13:29'),
(4,	21,	'The simson name',	2,	'A',	'2023-02-11 19:26:49');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `upassword` varchar(255) NOT NULL,
  `state` enum('A','I') NOT NULL DEFAULT 'A',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_type_id` (`user_type_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `users` (`user_id`, `user_type_id`, `name`, `email`, `upassword`, `state`, `date_register`) VALUES
(1,	1,	'andres',	'andrescastilloweb@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'A',	'2023-02-09 03:00:03'),
(14,	1,	'Maria Alejandra',	'alejandra92@gmail.com',	'',	'I',	'2023-02-11 20:41:49'),
(15,	1,	'Maria Alejandra',	'alejandra@gmail.com',	'f29d487d346740b0cf85938b78b135db',	'A',	'0000-00-00 00:00:00'),
(16,	1,	'Maria Alejandra',	'alejandra2@gmail.com',	'f29d487d346740b0cf85938b78b135db',	'A',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `user_types` (`user_type_id`, `name`) VALUES
(1,	'Administrator'),
(2,	'Employee'),
(3,	'Client');

-- 2023-02-11 11:45:19
