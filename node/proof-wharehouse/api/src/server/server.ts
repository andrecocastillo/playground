import express = require('express');
import Application from '../application/modules/core/constants/Application';
import BaseRouter from '../application/routes';
import cors = require('cors');
import { createConnection } from 'typeorm';
import { checkJwt, headers } from '../application/modules/core/middlewares/Cors';


const app: express.Application = express();
const bodyParser = require('body-parser');

// Others
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());
app.use((req, res, next) => headers(req,res,next));
app.use((req, res, next) => checkJwt(req,res,next));
app.use((req, res, next) => {
  if(!Application.is_instance_orm){
      Application.is_instance_orm = true;
      
      createConnection()
      .then(async connection => {
          console.log("Type ORM conectado");
          next();
      }).catch(error => {
         console.log("TypeORM connection error: ", error); 
         next();
      });
  }else{
      next();
  }
});


// ROUTES
app.get('/', function (req, res) {
  res.send('Welcome. To show core version visit. /core/version');
});
app.use('/', BaseRouter);

app.listen(3500, function () {
  console.log('App is listening on port 3500 !!');
});