import { Request, Response } from 'express'
import { StatusCodes } from 'http-status-codes';
import { getCustomRepository } from "typeorm";
import UserRepository from '../repositories/UserRepository';

class GeneralController {

    constructor(){}

    public async version(req: Request, res: Response){
        return res.status(StatusCodes.OK).json({
            res: true,
            dataObj: [],
            msg: "Version V 1.0.0"
        });
    }

    public async userTest(req: Request, res: Response){

        /*
            ONLY FOR TESTING PURPOSES
        */
       
        const usersRepository = getCustomRepository(UserRepository);
        let userInformation = await usersRepository.getInformationUser(1)
                                    .catch(e => { console.log("Error"); });

        return res.status(StatusCodes.OK).json({
            res: true,
            dataObj: [userInformation],
            msg: "Data generated successfully"
        });
    }
}

export default GeneralController