import { NextFunction } from "express";
import { StatusCodes } from "http-status-codes";
import * as jwt from "jsonwebtoken";
import Application from "../constants/Application";
import Headers from "../constants/Headers";

export const headers = (req: any,res: any,next: NextFunction) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    next();
};

export const checkJwt = (req: any,res: any,next: NextFunction) => {
    // Set headers data to global information
    if(req.url.indexOf('/login/access') != -1){
        next();
    }else{
        Headers.autorization = String(req.headers.authorization);

        // console.log( req.url.indexOf('/core/logs') );
        // if(
        //     req.url.indexOf('/core/logs') != -1 
        // ){
        //     next();
        // }
    
        try{
            jwt.verify(Headers.autorization, Application.jwtSecret);
            let token_information:any = jwt.decode(Headers.autorization);
            
            if(
                (
                    token_information.type == 'USER'
                )
            ){
                next(); 
            }else{
                throw new Error('Wrong token');
            }
        }catch(err){
            console.log(err);
            return res.status(StatusCodes.CONFLICT).json({
                res: false,
                dataObj: [req.url, err],
                msg: "Wrong token"
            });
        }
    }
};