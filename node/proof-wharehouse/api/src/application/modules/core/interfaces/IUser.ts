export interface IUser{
    user_id: number
    name: string
    email: string
    upassword: string
    date_register: Date
}