import { validate } from "validate-typescript";

class ValidateDataService{

    private rules_validation: any;
    private object_entity: any;

    constructor(rules_validation:any, object_entity:any){
        this.rules_validation = rules_validation;
        this.object_entity = object_entity;
    }

    getResults(){
        try{
            validate(this.rules_validation, this.object_entity);
            return {result:true};
        }catch(e:any){
            console.log('Error:', e);
            return {result:false};
        }
    }
}
export default ValidateDataService;