import * as jwt from "jsonwebtoken";
import Application from "../constants/Application";
import Headers from "../constants/Headers";

class Jwtsing{

    generateSing(user_id:number){
        const token_string = jwt.sign(
            { user_id: user_id, type: 'USER' },
            Application.jwtSecret,
            { expiresIn: "6h" }
        );
        
        return token_string;
    }

    getDataToken(){
        let information = jwt.decode(Headers.autorization);
        return information;
    }
    
}
export default Jwtsing;