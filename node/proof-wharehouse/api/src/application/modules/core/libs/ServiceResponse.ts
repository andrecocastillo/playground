import { StatusCodes } from "http-status-codes";

class ServiceResponse{

    private response_status: boolean = false;
    private response_data_object: any;
    private response_message: string = '';
    private status_code: any;

    constructor(){ }

    createSussesProcces(response_data_object:any,response_message:string){
        this.response_status = true;
        this.status_code = StatusCodes.OK;
        this.response_data_object = response_data_object;
        this.response_message = response_message;
    }

    createErrorProcces(response_data_object:any,response_message:string){
        this.response_status = false;
        this.status_code = StatusCodes.BAD_REQUEST;
        this.response_data_object = response_data_object;
        this.response_message = response_message;
        throw new Error(response_message);
    }

    setRequestError(error:Error){
        this.response_status = false;
        this.status_code = StatusCodes.BAD_REQUEST;
        this.response_data_object = {};
        this.response_message = error.message;
    }

    getResponseDataObject(){
        return this.response_data_object;
    }
    
    response(res:any){
        return res.status(StatusCodes.OK).json({
            res: this.response_status,
            dataObj: this.response_data_object,
            msg: this.response_message
        });
    }

}

export default ServiceResponse;