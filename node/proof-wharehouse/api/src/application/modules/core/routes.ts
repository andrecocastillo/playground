import { Router } from 'express';
import GeneralController from './controllers/GeneralController';

const router = Router();
const general_controller:GeneralController = new GeneralController();

router.get('/version', general_controller.version);
router.get('/user-test', general_controller.userTest);

export default router;