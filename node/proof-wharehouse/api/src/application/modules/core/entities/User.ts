import { Entity, PrimaryGeneratedColumn, Column,  BaseEntity, OneToOne, JoinTable } from "typeorm";

/*
    ONLY FOR TESTING PURPOSES
*/

@Entity({ name: "users" })
export class User extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    user_id: number = 0;

    @Column()
    name: string = '';

    @Column()
    email: string = ''

    @Column()
    upassword: string = '';

    @Column()
    date_register: Date = new Date();
}

export default User;