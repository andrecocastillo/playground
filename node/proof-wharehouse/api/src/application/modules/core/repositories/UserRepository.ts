import { EntityRepository, Repository } from "typeorm";
import User from "../entities/User";
import { IUser } from "../interfaces/IUser";
/*
    ONLY FOR TESTING PURPOSES
*/


@EntityRepository(User)
export class UserRepository extends Repository<IUser> {

    async getInformationUser(userId:number){
        return User.findOne({
            where: {
                user_id: userId,
            },
        });
    }
}

export default UserRepository