import { Request, Response } from 'express'
import SaveUserModel from '../models/SaveModel';
import UpdateModel from '../models/UpdateModel';
import RegisterUserRequest from '../request/RegisterUserRequest';
import UpdateUserRequest from '../request/UpdateUserRequest';
import DeleteUserService from '../services/DeleteUserService';
import InformationUserService from '../services/InformationUserService';
import ListUserService from '../services/ListUserService';
import SaveUserService from '../services/SaveUserService';
import UpdateUserService from '../services/UpdateUserService';

class GeneralController {

    constructor(){}

    public async register(req: Request, res: Response){
        let saveService = new SaveUserService();

        try{
            new RegisterUserRequest(req.body);

            await saveService.save(new SaveUserModel(req.body));
            return saveService.response(res);
        }catch(e:any){
            saveService.setRequestError(e);
            return saveService.response(res);
        }
    }

    public async getInformation(req: Request, res: Response){
        let informationService = new InformationUserService();
        
        try{
            await informationService.getInformation(parseInt(req.params.userId));
            return informationService.response(res);
        }catch(e:any){
            informationService.setRequestError(e);
            return informationService.response(res);
        }
    }

    public async updateProduct(req: Request, res: Response){
        let updateService = new UpdateUserService();

        try{
            new UpdateUserRequest(req.body);

            await updateService.update(new UpdateModel(req.body));
            return updateService.response(res);
        }catch(e:any){
            updateService.setRequestError(e);
            return updateService.response(res);
        }
    }

    public async listUsers(req: Request, res: Response){
        let listService = new ListUserService();
        
        try{
            await listService.list();
            return listService.response(res);
        }catch(e:any){
            listService.setRequestError(e);
            return listService.response(res);
        }
    }

    public async deleteUser(req: Request, res: Response){
        let deleteService = new DeleteUserService();
        
        try{
            await deleteService.delete(parseInt(req.params.userId));
            return deleteService.response(res);
        }catch(e:any){
            deleteService.setRequestError(e);
            return deleteService.response(res);
        }
    }
}

export default GeneralController