import { All, Email, RegEx } from "validate-typescript";
import ValidateDataService from "../../core/libs/ValidateDataService";
import CommonValidationRequest from './CommonValidationRequest';

class RegisterUserRequest extends CommonValidationRequest{
  
  constructor(request_data:any){
    super();

    const rules_validation = {
      // name:  All([RegEx(/^(?!\s*$).+/)]),
      // email: All([RegEx(/^(?!\s*$).+/)]),
      // password: All([RegEx(/^(?!\s*$).+/)]),
      // typeUser: All([RegEx(/^(?!\s*$).+/)])
    };
    
    let validate_object:any = new ValidateDataService(rules_validation, request_data);
    let validations_fields:any = validate_object.getResults();

    if(validations_fields.result == false){
      throw new Error(this.standar_message);
    }
  }
}

export default RegisterUserRequest