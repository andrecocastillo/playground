import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import UpdateModel from '../models/UpdateModel';
import UserRepository from '../repositories/UserRepository';

class UpdateUserService  extends ServiceResponse{

    public async update(userData:UpdateModel){
        const productRepository = getCustomRepository(UserRepository);

        await productRepository.updateProduct(userData)
                .catch(e => this.createErrorProcces([e], LANG.general.dbError));

        this.createSussesProcces({}, LANG.users.userUpdated);
    }
}

export default UpdateUserService