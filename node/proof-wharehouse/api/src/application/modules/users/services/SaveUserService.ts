import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import SaveUserModel from '../models/SaveModel';
import UserRepository from '../repositories/UserRepository';

class SaveUserService  extends ServiceResponse{

    public async save(productsData:SaveUserModel){
        const productRepository = getCustomRepository(UserRepository);

        await productRepository.saveUser(productsData)
                .catch(e => this.createErrorProcces([e], LANG.general.dbError));

        this.createSussesProcces({}, LANG.users.userSaved);
    }
}

export default SaveUserService