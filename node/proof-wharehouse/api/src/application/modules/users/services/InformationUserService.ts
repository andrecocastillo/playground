import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import UserRepository from '../repositories/UserRepository';

class InformationUserService  extends ServiceResponse{

    public async getInformation(userId:number){
        const userRepository = getCustomRepository(UserRepository);

        let produt:any = await userRepository.informationUser(userId)
                            .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces(produt, LANG.users.userInformation);
    }
}

export default InformationUserService