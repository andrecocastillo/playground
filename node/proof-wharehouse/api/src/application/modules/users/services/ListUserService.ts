import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import UserRepository from '../repositories/UserRepository';


class ListUserService  extends ServiceResponse{

    public async list(){
        const productRepository = getCustomRepository(UserRepository);

        let produt:any = await productRepository.allUsers()
                .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces(produt, LANG.users.userList);
    }
}

export default ListUserService