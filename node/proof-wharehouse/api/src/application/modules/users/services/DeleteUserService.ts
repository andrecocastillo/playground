import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import UpdateModel from '../models/UpdateModel';
import UserRepository from '../repositories/UserRepository';

class DeleteUserService  extends ServiceResponse{

    public async delete(userId:number){
        const productRepository = getCustomRepository(UserRepository);

        await productRepository.deleteUser(userId)
                .catch(e => this.createErrorProcces([e], LANG.general.dbError));

        this.createSussesProcces({}, LANG.users.userDeleted);
    }
}

export default DeleteUserService