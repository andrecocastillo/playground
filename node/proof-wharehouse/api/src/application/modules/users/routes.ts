import { Router } from 'express';
import GeneralController from './controllers/GeneralController';

const router = Router();
const general_controller:GeneralController = new GeneralController();

router.post('/register', general_controller.register);
router.get('/get-information/:userId', general_controller.getInformation);
router.put('/update', general_controller.updateProduct);
router.delete('/delete/:userId', general_controller.deleteUser);
router.get('/list', general_controller.listUsers);

export default router;