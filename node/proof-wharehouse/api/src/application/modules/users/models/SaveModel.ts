export class SaveUserModel{

  private name:string = '';
  private email:string = '';
  private password:string = '';
  private typeUser:number = 0;


  constructor(request_data:any){
    this.name      = request_data.name;
    this.email  = request_data.email;
    this.password = request_data.password;
    this.typeUser = request_data.typeUser;
  }

  get_name(): string  {
    return this.name;
  }

  set_name(value: string) {
		this.name = value;
	}

  get_email(): string  {
    return this.email;
  }

  set_email(value: string) {
		this.name = value;
	}

  get_password(): string  {
    return this.password;
  }

  set_password(value: string) {
		this.password = value;
	}

  get_typeUser(): number  {
    return this.typeUser;
  }

  set_typeUser(value: number) {
		this.typeUser = value;
	}
}

export default SaveUserModel