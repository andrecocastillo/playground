import { Md5 } from "md5-typescript";
import { EntityRepository, Repository } from "typeorm";
import User from "../entities/User";
import { IUser } from "../interfaces/IUser";
import SaveUserModel from "../models/SaveModel";
import UpdateModel from "../models/UpdateModel";

@EntityRepository(User)
class UserRepository extends Repository<IUser> {

    async saveUser(userData:SaveUserModel){
        let passwordEncripted = Md5.init(userData.get_password());

        let information:any = {};
        information.name    = userData.get_name();
        information.email    = userData.get_email();
        information.state    = 'A';
        information.user_type_id    = userData.get_typeUser();
        information.upassword    = passwordEncripted;

        await this.save(information);
    }

    async updateProduct(userData:UpdateModel){
        let user:any = await this.findOne(userData.get_id());
        
        if(user){
            user.name    = userData.get_name();
            user.email    = userData.get_email();
            user.user_type_id    = userData.get_typeUser();

            await this.save(user);
        }
    }

    async informationUser(userId:number){
        return await this.findOne(userId);
    }

    async deleteUser(userId:number){
        let user:any = await this.findOne(userId);
        
        if(user){
            user.state = 'I';
            await this.save(user);
        }
    }

    async allUsers(){
        let result =  await this.find({
            where: {
                    state: 'A'
            }
        });
        
        return result;
    }
}

export default UserRepository