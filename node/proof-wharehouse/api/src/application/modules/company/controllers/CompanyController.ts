import { Request, Response } from 'express'
import CompanyModel from '../models/CompanyModel';
import RegisterCompanyRequest from '../request/RegisterCompanyRequest';
import DeleteCompanyService from '../services/DeleteCompanyService';
import GetInformationCompanyService from '../services/GetInformationCompanyService';
import ListCompanyService from '../services/ListCompanyService';
import SaveCompanyService from '../services/SaveCompanyService';
import UpdateCompanyService from '../services/UpdateCompanyService';

class CompanyController {

    constructor(){}

    public async saveCompany(req: Request, res: Response){

        let saveService = new SaveCompanyService();
        try{
            new RegisterCompanyRequest(req.body);

            await saveService.saveCompany(new CompanyModel(req.body));
            return saveService.response(res);
        }catch(e:any){
            saveService.setRequestError(e);
            return saveService.response(res);
        }
    }

    public async updateCompany(req: Request, res: Response){

        let updateService = new UpdateCompanyService();
        let params:  any = req.params;
        try{
            new RegisterCompanyRequest(req.body);

            await updateService.updateCompany(new CompanyModel(req.body));
            return updateService.response(res);
        }catch(e:any){
            updateService.setRequestError(e);
            return updateService.response(res);
        }
    }

    public async listCompanies(req: Request, res: Response){
        let listService = new ListCompanyService();

        try{
            await listService.listCompanies();
            return listService.response(res);
        }catch(e:any){
            listService.setRequestError(e);
            return listService.response(res);
        }

    }

    public async informationCompany(req: Request, res: Response){

        let informationService = new GetInformationCompanyService();
        let params:  any = req.params;

        try{
            await informationService.informationCompany(params.company_id);
            return informationService.response(res);
        }catch(e:any){
            informationService.setRequestError(e);
            return informationService.response(res);
        }
    }

    public async deleteCompany(req: Request, res: Response){

        let deleteService = new DeleteCompanyService();
        let params:  any = req.params;

        try{
            await deleteService.deleteCompany(params.company_id);
            return deleteService.response(res);
        }catch(e:any){
            deleteService.setRequestError(e);
            return deleteService.response(res);
        }
    }
}

export default CompanyController