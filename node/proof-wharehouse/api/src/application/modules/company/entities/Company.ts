import { Entity, PrimaryGeneratedColumn, Column,  BaseEntity, OneToOne, JoinTable } from "typeorm";

@Entity({ name: "companies" })
export class Company extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    company_id: number = 0;

    @Column()
    name: string = '';

    @Column()
    address: string = '';

    @Column()
    nit: number = 0;

    @Column()
    phone: number = 0;

    @Column()
    state: string = '';

    @Column()
    created: Date = new Date();

}

export default Company;