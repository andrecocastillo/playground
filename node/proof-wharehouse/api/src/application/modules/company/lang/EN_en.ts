const COMPANY_LANG = {
    "welcome": "Welcome !",
    "queryError": "The query process could not be performed.",
    "msgSaveSuccessfully": "It was successfully registered.",
    "msgListSuccessfully": "Successful listing.",
    "msgGetSuccessfully": "Information found successfully.",
    "msgDataUpdateSuccessfully": "Information updated successfully..",
    "msgNoDataSuccessfully": "No results found.",
    "msgDeleteData": "Information removed successfully.",
    "userAccesDenied": "The user does not match in our system",
};

export default COMPANY_LANG;