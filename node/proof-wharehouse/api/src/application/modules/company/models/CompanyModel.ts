export class CompanyModel{

    private company_id:number;
    private name:string = '';
    private address:string = '';
    private nit:number;
    private phone:number;
  
    constructor(request_data:any){
      this.company_id = request_data.company_id;
      this.name       = request_data.name;
      this.address    = request_data.address;
      this.nit        = request_data.nit;
      this.phone      = request_data.phone;
    }

    get_company_id(): number  {
      return this.company_id;
    }

    set_company_id(value: number ) {
      this.company_id = value;
    }
    
    get_id(): number  {
      return this.company_id;
    }

    set_id(value: number ) {
      this.company_id = value;
    }
  
    get_name(): string  {
        return this.name;
    }
  
    set_name(value: string ) {
      this.name = value;
    }

    get_address(): string  {
      return this.address;
    }

    set_address(value: string ) {
      this.address = value;
    }

    get_nit(): number  {
      return this.nit;
    }

    set_nit(value: number ) {
      this.nit = value;
    }

    get_phone(): number  {
      return this.phone;
    }

    set_phone(value: number ) {
      this.phone = value;
    }

  }
  
  export default CompanyModel