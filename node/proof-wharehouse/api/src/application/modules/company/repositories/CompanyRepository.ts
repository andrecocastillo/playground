import { EntityRepository, Repository } from "typeorm";
import Company from "../entities/Company";
import { ISaveCompany } from "../interfaces/ISaveCompany";
import CompanyModel from "../models/CompanyModel";

@EntityRepository(Company)
export class CompanyRepository extends Repository<ISaveCompany> {
    
    async saveCompany(companyModel:CompanyModel){
        
        let data_to_save:ISaveCompany = {
            name: companyModel.get_name(),
            address: companyModel.get_address(),
            nit: companyModel.get_nit(),
            phone: companyModel.get_phone(),
            created: new Date(),
        };

        this.save(data_to_save);
    }

    async updateCompany(companyModel:CompanyModel){
        let information = await this.findOne(companyModel.get_company_id());
        
        if(information){
            information.name    = companyModel.get_name();
            information.address = companyModel.get_address();
            information.phone   = companyModel.get_phone();
            information.nit     = companyModel.get_nit();
            await this.save(information);
        }
    }

    async listCompaniesActive(){
        let list = await this.find({ where: {state: "A"}});
        return list;
    }

    async getInformationCompany(companyId:number){
        return await this.findOne({
                        where: {
                            company_id: companyId,
                        }
                    });
    }

    async updateCompanyStatus(companyId:number){

        let information = await this.findOne(companyId);
        
        if(information){
            information.state = "I";
            await this.save(information);
        }
    }
}

export default CompanyRepository