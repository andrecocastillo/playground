import { Router } from 'express';
import CompanyController from './controllers/CompanyController';

const router = Router();
const company_controller:CompanyController = new CompanyController();

router.post('/save', company_controller.saveCompany);
router.get('/list', company_controller.listCompanies);
router.get('/get-information/:company_id', company_controller.informationCompany);
router.delete('/delete/:company_id', company_controller.deleteCompany);
router.put('/update', company_controller.updateCompany);

export default router;