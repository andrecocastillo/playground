export interface ISaveCompany{
    company_id?: number
    name?: string
    address?: string
    nit?: number
    phone?: number
    state?: string
    created?: Date
}