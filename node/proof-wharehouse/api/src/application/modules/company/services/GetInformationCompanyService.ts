import { getCustomRepository } from "typeorm";
import ServiceResponse from '../../core/libs/ServiceResponse';
import COMPANY_LANG from "../lang/EN_en";
import CompanyRepository from "../repositories/CompanyRepository";


class GetInformationCompanyService extends ServiceResponse {

    constructor(){ super(); }

    public async informationCompany(companyId:number){

        const companyRepository = getCustomRepository(CompanyRepository);

        let information_company = await companyRepository.getInformationCompany(companyId)
                                .catch(e => this.createErrorProcces([], COMPANY_LANG.queryError));

        if(information_company){
            this.createSussesProcces(information_company, COMPANY_LANG.msgGetSuccessfully);
        }else{
            this.createErrorProcces({}, COMPANY_LANG.msgNoDataSuccessfully); 
        }
        
    }
}

export default GetInformationCompanyService