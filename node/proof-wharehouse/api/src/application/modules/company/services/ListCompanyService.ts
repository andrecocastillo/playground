import { getCustomRepository } from "typeorm";
import ServiceResponse from '../../core/libs/ServiceResponse';
import COMPANY_LANG from "../lang/EN_en";
import CompanyRepository from "../repositories/CompanyRepository";


class ListCompanyService extends ServiceResponse {

    constructor(){ super(); }

    public async listCompanies(){
        const companyRepository = getCustomRepository(CompanyRepository);

        let list_Companies = await companyRepository.listCompaniesActive()
                                .catch(e => this.createErrorProcces([], COMPANY_LANG.queryError));

        if(list_Companies){
            this.createSussesProcces(list_Companies, COMPANY_LANG.msgListSuccessfully);
        }else{
            this.createErrorProcces({}, COMPANY_LANG.msgNoDataSuccessfully); 
        }
    }

}

export default ListCompanyService