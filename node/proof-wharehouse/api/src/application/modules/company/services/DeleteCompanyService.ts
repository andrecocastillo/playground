import { getCustomRepository } from "typeorm";
import ServiceResponse from '../../core/libs/ServiceResponse';
import COMPANY_LANG from "../lang/EN_en";
import CompanyRepository from "../repositories/CompanyRepository";


class DeleteCompanyService extends ServiceResponse {

    constructor(){ super(); }

    public async deleteCompany(companyId:number){

        const companyRepository = getCustomRepository(CompanyRepository);

        await companyRepository.updateCompanyStatus(companyId)
                                .catch(e => this.createErrorProcces([], COMPANY_LANG.queryError));
        this.createSussesProcces({}, COMPANY_LANG.msgDeleteData);
        
        
    }
}

export default DeleteCompanyService