import { getCustomRepository } from "typeorm";
import ServiceResponse from '../../core/libs/ServiceResponse';
import COMPANY_LANG from "../lang/EN_en";
import CompanyModel from "../models/CompanyModel";
import CompanyRepository from "../repositories/CompanyRepository";


class UpdateCompanyService extends ServiceResponse {

    constructor(){ super(); }

    public async updateCompany(companyModel:CompanyModel){

        const companyRepository = getCustomRepository(CompanyRepository);

        await companyRepository.updateCompany(companyModel)
                                .catch(e => this.createErrorProcces([], COMPANY_LANG.queryError));

        this.createSussesProcces({}, COMPANY_LANG.msgDataUpdateSuccessfully);
    }
    
}

export default UpdateCompanyService