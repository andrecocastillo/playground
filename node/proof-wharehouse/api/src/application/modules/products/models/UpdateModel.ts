export class UpdateModel{

  private id:number;
  private name:string = '';
  private quantity:number;
  private companyId:number;
  private created  : Date;

  constructor(request_data:any){
    this.id       = request_data.id;
    this.name     = request_data.name;
    this.quantity = request_data.quantity;
    this.companyId = request_data.companyId;
    this.created  = request_data.created;
  }

  get_id(): number  {
    return this.id;
  }

  set_id(value: number ) {
		this.id = value;
	}

  get_name(): string  {
    return this.name;
  }

  set_name(value: string ) {
		this.name = value;
	}

  get_quantity(): number  {
    return this.quantity;
  }

  set_quantity(value: number ) {
		this.quantity = value;
	}

  get_companyId(): number  {
    return this.companyId;
  }

  set_companyId(value: number ) {
		this.companyId = value;
	}

  get_created(): Date  {
    return this.created;
  }

  set_created(value: Date ) {
		this.created = value;
	}

}

export default UpdateModel