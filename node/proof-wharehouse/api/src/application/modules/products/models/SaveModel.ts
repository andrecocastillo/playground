export class SaveModel{

  private name:string = '';
  private quantity:number;
  private companyId:number;

  constructor(request_data:any){
    this.name      = request_data.name;
    this.quantity  = request_data.quantity;
    this.companyId = request_data.companyId;
  }

  get_name(): string  {
    return this.name;
  }

  set_name(value: string ) {
		this.name = value;
	}

  get_quantity(): number  {
    return this.quantity;
  }

  set_quantity(value: number ) {
		this.quantity = value;
	}

  get_companyId(): number  {
    return this.companyId;
  }

  set_companyId(value: number ) {
		this.companyId = value;
	}
}

export default SaveModel