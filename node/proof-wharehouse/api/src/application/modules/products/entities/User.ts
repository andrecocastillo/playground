import { Entity, PrimaryGeneratedColumn, Column,  BaseEntity, OneToOne, JoinTable } from "typeorm";

@Entity({ name: "products" })
export class Product extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    product_id: number = 0;

    @Column()
    company_id:string = '';

    @Column()
    name:string = '';

    @Column()
    quantity:string = '';

    @Column()
    state:string = '';

    @Column()
    created: Date = new Date();
}

export default Product;