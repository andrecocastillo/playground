import { Request, Response } from 'express'
import SaveProductService from '../services/SaveProductService';
import UpdateProductService from '../services/UpdateProductService';
import ListProductService from '../services/ListProductService';
import DeleteProductService from '../services/DeleteProductService';
import RegisterProductRequest from '../request/RegisterProductRequest';
import UpdateProductRequest from '../request/UpdateProductRequest';
import SaveModel from '../models/SaveModel';
import UpdateModel from '../models/UpdateModel';
import InformationProductService from '../services/InformationProductService';

class GeneralController {

    constructor(){}

    public async registerProduct(req: Request, res: Response){
        let saveService = new SaveProductService();

        try{
            new RegisterProductRequest(req.body);

            await saveService.save(new SaveModel(req.body));
            return saveService.response(res);
        }catch(e:any){
            saveService.setRequestError(e);
            return saveService.response(res);
        }
    }

   public async updateProduct(req: Request, res: Response){
        let updateService = new UpdateProductService();

        try{
            new UpdateProductRequest(req.body);

            await updateService.update(new UpdateModel(req.body));
            return updateService.response(res);
        }catch(e:any){
            updateService.setRequestError(e);
            return updateService.response(res);
        }
    }

    public async listProduct(req: Request, res: Response){
        let listService = new ListProductService();
        
        try{
            await listService.list();
            return listService.response(res);
        }catch(e:any){
            listService.setRequestError(e);
            return listService.response(res);
        }
    }

    public async deleteProduct(req: Request, res: Response){
        let deleteService = new DeleteProductService();
        
        try{
            await deleteService.delete(parseInt(req.params.productId));
            return deleteService.response(res);
        }catch(e:any){
            deleteService.setRequestError(e);
            return deleteService.response(res);
        }
    }

    public async getInformation(req: Request, res: Response){
        let informationProduct = new InformationProductService();
        
        try{
            await informationProduct.getInformation(parseInt(req.params.productId));
            return informationProduct.response(res);
        }catch(e:any){
            informationProduct.setRequestError(e);
            return informationProduct.response(res);
        }
    }
}

export default GeneralController