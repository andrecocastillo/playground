import { Request, Response } from 'express'
import InventoryCompanyService from '../services/InventoryCompanyService';


class CompanyController {

    constructor(){}

    public async generatePdf(req: Request, res: Response){
        let saveService = new InventoryCompanyService();

        try{
            await saveService.generatePdf(parseInt(req.params.companyId));
            return saveService.response(res);
        }catch(e:any){
            saveService.setRequestError(e);
            return saveService.response(res);
        }
    }

}

export default CompanyController