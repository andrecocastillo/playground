export interface IProduct{
    user_id: number
    company_id:string
    name:string
    quantity:string
    state:string
    created: Date
}