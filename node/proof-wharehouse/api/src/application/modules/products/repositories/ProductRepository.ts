import { Md5 } from "md5-typescript";
import { EntityRepository, Repository } from "typeorm";
import { Product } from "../entities/User";
import { IProduct } from "../interfaces/IProduct";
import SaveModel from "../models/SaveModel";
import UpdateModel from "../models/UpdateModel";


@EntityRepository(Product)
class ProductRepository extends Repository<IProduct> {

    async saveProduct(productsData:SaveModel){
        let information:any = {};
        information.company_id    = productsData.get_companyId();
        information.name    = productsData.get_name();
        information.quantity    = productsData.get_quantity();
        information.created    = new Date();

        await this.save(information);
    }

    async updateProduct(productsData:UpdateModel){
        let produt = await this.findOne(productsData.get_id());
        
        if(produt){
            let information:any = {};
            information.product_id    = productsData.get_id();
            information.company_id    = productsData.get_companyId();
            information.name    = productsData.get_name();
            information.quantity    = productsData.get_quantity();
            information.created    = new Date();

            await this.save(information);
        }
    }

    async informationProduct(productId:number){
        return await this.findOne(productId);
    }

    async allProducts(){
        let result =  await this.find({
            where: {
                    state: 'A'
            }
        });

        return result;
    }

    async deleteProduct(productId:number){
        let produt = await this.findOne(productId);
        if(produt){
            produt.state = 'I';
            await this.save(produt);
        }
    }

    async getProductsCompany(companyId:number){
        let result =  await this.find({
            where: {
                company_id: companyId
            }
        });

        return result;
    }
}

export default ProductRepository