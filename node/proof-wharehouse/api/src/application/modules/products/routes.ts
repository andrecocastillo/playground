import { Router } from 'express';
import CompanyController from './controllers/CompanyController';
import GeneralController from './controllers/GeneralController';

const router = Router();
const general_controller:GeneralController = new GeneralController();
const company_controller:CompanyController = new CompanyController();

router.post('/register', general_controller.registerProduct);
router.put('/update', general_controller.updateProduct);
router.get('/list', general_controller.listProduct);
router.delete('/delete/:productId', general_controller.deleteProduct);
router.get('/get-information/:productId', general_controller.getInformation);
router.get('/generate-pdf-company/:companyId', company_controller.generatePdf);

export default router;