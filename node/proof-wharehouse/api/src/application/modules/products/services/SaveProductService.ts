import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import SaveModel from '../models/SaveModel';
import ProductRepository from '../repositories/ProductRepository';

class SaveProductService  extends ServiceResponse{

    public async save(productsData:SaveModel){
        const productRepository = getCustomRepository(ProductRepository);

        await productRepository.saveProduct(productsData)
                .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces({}, LANG.products.productSaved);
    }
}

export default SaveProductService