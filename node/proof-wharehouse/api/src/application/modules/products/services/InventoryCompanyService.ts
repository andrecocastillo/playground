import puppeteer from 'puppeteer';
import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import ProductRepository from '../repositories/ProductRepository';

class InventoryCompanyService  extends ServiceResponse{

    public async generatePdf(companyId:number){

        const productRepository = getCustomRepository(ProductRepository);

        let listProducts:any = await productRepository.getProductsCompany(companyId)
                                    .catch(e => this.createErrorProcces([], LANG.general.dbError));


        let items = ``;
        for(const pruductInfo of listProducts){
            items += `
                    <tr>
                        <td>`+ pruductInfo.name +`</td>
                        <td>`+ pruductInfo.quantity +`</td>
                    </tr>
            `;
        }

        let viewHTML = `
        <table border="1" cellpadding="1" cellspacing="1" style="width:100%">
            <tbody>
                <tr>
                    <td> PRODUCT </td>
                    <td> QUIANTITY </td>
                </tr>
                ` + items + `
            </tbody>
        </table>
        `;

        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.setContent(viewHTML, { waitUntil: 'domcontentloaded' });
        await page.emulateMediaType('screen');
        const pdf = await page.pdf({
            path: './pdf/company-inventory-'+ companyId +'.pdf',
            margin: { top: '100px', right: '50px', bottom: '100px', left: '50px' },
            printBackground: true,
            format: 'A4',
          });

        await browser.close();

        this.createSussesProcces({}, LANG.products.pdfGenerated);
    }
}

export default InventoryCompanyService