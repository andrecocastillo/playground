import { getCustomRepository } from 'typeorm';
import { LANG } from '../../../lang/EN_en';
import ServiceResponse from '../../core/libs/ServiceResponse';
import ProductRepository from '../repositories/ProductRepository';

class InformationProductService  extends ServiceResponse{

    public async getInformation(productId:number){
        const productRepository = getCustomRepository(ProductRepository);

        let produt:any = await productRepository.informationProduct(productId)
                .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces(produt, LANG.products.producInformation);
    }
}

export default InformationProductService