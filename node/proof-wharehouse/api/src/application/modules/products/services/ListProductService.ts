import ServiceResponse from '../../core/libs/ServiceResponse';
import Jwtsing from "../../core/libs/Jwtsing";
import { LANG } from '../../../lang/EN_en';
import { getCustomRepository } from 'typeorm';
import ProductRepository from '../repositories/ProductRepository';

class ListProductService  extends ServiceResponse{

    public async list(){
        const productRepository = getCustomRepository(ProductRepository);

        let produt:any = await productRepository.allProducts()
                            .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces(produt, LANG.products.produtList);
    }
}

export default ListProductService