import ServiceResponse from '../../core/libs/ServiceResponse';
import Jwtsing from "../../core/libs/Jwtsing";
import { getCustomRepository } from 'typeorm';
import ProductRepository from '../repositories/ProductRepository';
import UpdateModel from '../models/UpdateModel';
import { LANG } from '../../../lang/EN_en';

class UpdateProductService  extends ServiceResponse{

    public async update(produtInformation:UpdateModel){
        const productRepository = getCustomRepository(ProductRepository);

       await productRepository.updateProduct(produtInformation)
                .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces({}, LANG.products.productUpdated);
    }
}

export default UpdateProductService