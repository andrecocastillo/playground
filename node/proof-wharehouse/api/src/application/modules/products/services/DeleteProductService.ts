import ServiceResponse from '../../core/libs/ServiceResponse';
import { getCustomRepository } from 'typeorm';
import ProductRepository from '../repositories/ProductRepository';
import { LANG } from '../../../lang/EN_en';

class DeleteUserService  extends ServiceResponse{

    public async delete(productId:number){
        const productRepository = getCustomRepository(ProductRepository);

        await productRepository.deleteProduct(productId)
                .catch(e => this.createErrorProcces([], LANG.general.dbError));

        this.createSussesProcces({}, LANG.users.userDeleted);
    }
}

export default DeleteUserService