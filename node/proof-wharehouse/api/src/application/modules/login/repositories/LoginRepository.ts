import { Md5 } from "md5-typescript";
import { EntityRepository, Repository } from "typeorm";
import User from "../../users/entities/User";
import { IUserLogin } from "../../users/interfaces/IUserLogin";
import AccesModel from "../models/AccesModel";

@EntityRepository(User)
export class LoginRepository extends Repository<IUserLogin> {

    
    async login(accesUser:AccesModel){
        let passwordEncripted = Md5.init(accesUser.get_password());

        return User.findOne({
                        where: {
                            email: accesUser.get_username(),
                            upassword: passwordEncripted
                        },
                    });
    }

}

export default LoginRepository