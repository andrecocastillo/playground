import { getCustomRepository } from "typeorm";
import ServiceResponse from '../../core/libs/ServiceResponse';
import LOGIN_LANG from "../lang/EN_en";
import UpdateMapModel, { AccesModel } from '../models/AccesModel';
import LoginRepository from "../repositories/LoginRepository";
import Jwtsing from "../../core/libs/Jwtsing";

class LoginService extends ServiceResponse {

    constructor(){ super(); }

    public async access(updateModel:AccesModel){
        const footerRepository = getCustomRepository(LoginRepository);

        let userLoginInfo = await footerRepository.login(updateModel)
                                .catch(e => this.createErrorProcces([], LOGIN_LANG.queryError));

        if(userLoginInfo){
            let objToken = new Jwtsing();
            let tokenString = objToken.generateSing(userLoginInfo.user_id);

            this.createSussesProcces({tokenString}, LOGIN_LANG.welcome);
        }else{
            this.createErrorProcces({}, LOGIN_LANG.userAccesDenied);
        }
    }


    private async generateToken(){

    }
}

export default LoginService