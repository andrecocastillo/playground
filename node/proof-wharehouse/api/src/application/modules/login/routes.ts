import { Router } from 'express';
import GeneralController from './controllers/GeneralController';

const router = Router();
const general_controller:GeneralController = new GeneralController();

router.post('/access', general_controller.access);

export default router;