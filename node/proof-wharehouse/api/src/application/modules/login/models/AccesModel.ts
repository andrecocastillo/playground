export class AccesModel{

  private username:string = '';
  private password:string = '';

  constructor(request_data:any){
    this.username = request_data.username;
    this.password = request_data.password;
  }

  get_username(): string  {
      return this.username;
    }

  set_username(value: string ) {
		this.username = value;
	}

  get_password(): string  {
    return this.password;
  }

  set_password(value: string ) {
    this.username = value;
  }
}

export default AccesModel