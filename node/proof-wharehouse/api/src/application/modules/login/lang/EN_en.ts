const LOGIN_LANG = {
    "welcome": "Welcome !",
    "queryError": "Query error",
    "userAccesDenied": "The user does not match in our system"
};

export default LOGIN_LANG;