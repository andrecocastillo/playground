import { Request, Response } from 'express'
import AccesModel from '../models/AccesModel';
import AccessRequest from '../request/AccessRequest';
import LoginService from '../services/LoginService';

class GeneralController {

    constructor(){}

    public async access(req: Request, res: Response){
        let map_service = new LoginService();

        try{
            new AccessRequest(req.body);
            
            await map_service.access(new AccesModel(req.body));
            return map_service.response(res);
        }catch(e:any){
            map_service.setRequestError(e);
            return map_service.response(res);
        }
    }
}

export default GeneralController