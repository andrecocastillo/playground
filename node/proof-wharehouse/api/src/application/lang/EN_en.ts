export const LANG = {
    general:{
        dbError : "Error in Query"
    },
    products:{
        productSaved: "Registered product successfully.",
        producInformation: "Information product",
        producDeleted: "Product deleted",
        productUpdated: "Updated product successfully.",
        produtList: "List of all produts",
        pdfGenerated: "PDF generated"
    },
    users:{
        userSaved: "Registered user successfully.",
        userInformation: "Information product",
        userDeleted: "User deleted",
        userUpdated: "Updated user successfully.",
        userList: "List of all users"
    }
}