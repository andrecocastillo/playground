import { Router } from 'express';
import CoreRouter from '../modules/core/routes';
import LoginRouter from '../modules/login/routes';
import UsersRouter from '../modules/users/routes';
import CompanyRouter from '../modules/company/routes';
import ProductsRouter from '../modules/products/routes';

const router = Router();

router.use('/core/', CoreRouter);
router.use('/login/', LoginRouter);
router.use('/users/', UsersRouter);
router.use('/company/', CompanyRouter);
router.use('/products/', ProductsRouter);

export default router;