"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var routes_1 = __importDefault(require("../modules/core/routes"));
var routes_2 = __importDefault(require("../modules/login/routes"));
var routes_3 = __importDefault(require("../modules/users/routes"));
var routes_4 = __importDefault(require("../modules/company/routes"));
var routes_5 = __importDefault(require("../modules/products/routes"));
var router = (0, express_1.Router)();
router.use('/core/', routes_1.default);
router.use('/login/', routes_2.default);
router.use('/users/', routes_3.default);
router.use('/company/', routes_4.default);
router.use('/products/', routes_5.default);
exports.default = router;
