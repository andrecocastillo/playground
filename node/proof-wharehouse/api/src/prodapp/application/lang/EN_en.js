"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LANG = void 0;
exports.LANG = {
    general: {
        dbError: "Error in Query"
    },
    products: {
        productSaved: "Registered product successfully.",
        producInformation: "Information product",
        producDeleted: "Product deleted",
        productUpdated: "Updated product successfully.",
        produtList: "List of all produts"
    },
    users: {
        userSaved: "Registered user successfully.",
        userInformation: "Information product",
        userDeleted: "User deleted",
        userUpdated: "Updated user successfully.",
        userList: "List of all users"
    }
};
