"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccesModel = void 0;
var AccesModel = /** @class */ (function () {
    function AccesModel(request_data) {
        this.username = '';
        this.password = '';
        this.username = request_data.username;
        this.password = request_data.password;
    }
    AccesModel.prototype.get_username = function () {
        return this.username;
    };
    AccesModel.prototype.set_username = function (value) {
        this.username = value;
    };
    AccesModel.prototype.get_password = function () {
        return this.password;
    };
    AccesModel.prototype.set_password = function (value) {
        this.username = value;
    };
    return AccesModel;
}());
exports.AccesModel = AccesModel;
exports.default = AccesModel;
