"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LOGIN_LANG = {
    "welcome": "Welcome !",
    "queryError": "Query error",
    "userAccesDenied": "The user does not match in our system"
};
exports.default = LOGIN_LANG;
