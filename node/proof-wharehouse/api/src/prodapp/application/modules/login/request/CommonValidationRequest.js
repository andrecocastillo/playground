"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommonValidationRequest = void 0;
var CommonValidationRequest = /** @class */ (function () {
    function CommonValidationRequest() {
        this.standar_message = 'Error en los datos de ingreso';
    }
    return CommonValidationRequest;
}());
exports.CommonValidationRequest = CommonValidationRequest;
exports.default = CommonValidationRequest;
