"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ValidateDataService_1 = __importDefault(require("../../core/libs/ValidateDataService"));
var CommonValidationRequest_1 = __importDefault(require("./CommonValidationRequest"));
var UpdateUserRequest = /** @class */ (function (_super) {
    __extends(UpdateUserRequest, _super);
    function UpdateUserRequest(request_data) {
        var _this = _super.call(this) || this;
        var rules_validation = {
        // id:  All([RegEx(/^(?!\s*$).+/)]),
        // name:  All([RegEx(/^(?!\s*$).+/)]),
        // email: All([RegEx(/^(?!\s*$).+/)]),
        // password: All([RegEx(/^(?!\s*$).+/)]),
        // typeUser: All([RegEx(/^(?!\s*$).+/)])
        };
        var validate_object = new ValidateDataService_1.default(rules_validation, request_data);
        var validations_fields = validate_object.getResults();
        if (validations_fields.result == false) {
            throw new Error(_this.standar_message);
        }
        return _this;
    }
    return UpdateUserRequest;
}(CommonValidationRequest_1.default));
exports.default = UpdateUserRequest;
