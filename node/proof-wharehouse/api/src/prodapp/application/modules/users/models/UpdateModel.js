"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateModel = void 0;
var UpdateModel = /** @class */ (function () {
    function UpdateModel(request_data) {
        this.id = 0;
        this.name = '';
        this.email = '';
        this.password = '';
        this.typeUser = 0;
        this.id = request_data.id;
        this.name = request_data.name;
        this.email = request_data.email;
        this.password = request_data.password;
        this.typeUser = request_data.typeUser;
    }
    UpdateModel.prototype.get_id = function () {
        return this.id;
    };
    UpdateModel.prototype.set_id = function (value) {
        this.id = value;
    };
    UpdateModel.prototype.get_name = function () {
        return this.name;
    };
    UpdateModel.prototype.set_name = function (value) {
        this.name = value;
    };
    UpdateModel.prototype.get_email = function () {
        return this.email;
    };
    UpdateModel.prototype.set_email = function (value) {
        this.name = value;
    };
    UpdateModel.prototype.get_password = function () {
        return this.password;
    };
    UpdateModel.prototype.set_password = function (value) {
        this.password = value;
    };
    UpdateModel.prototype.get_typeUser = function () {
        return this.typeUser;
    };
    UpdateModel.prototype.set_typeUser = function (value) {
        this.typeUser = value;
    };
    return UpdateModel;
}());
exports.UpdateModel = UpdateModel;
exports.default = UpdateModel;
