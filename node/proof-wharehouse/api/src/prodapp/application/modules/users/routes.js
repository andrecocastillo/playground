"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var GeneralController_1 = __importDefault(require("./controllers/GeneralController"));
var router = (0, express_1.Router)();
var general_controller = new GeneralController_1.default();
router.post('/register', general_controller.register);
router.get('/get-information/:userId', general_controller.getInformation);
router.put('/update', general_controller.updateProduct);
router.delete('/delete/:userId', general_controller.deleteUser);
router.get('/list', general_controller.listUsers);
exports.default = router;
