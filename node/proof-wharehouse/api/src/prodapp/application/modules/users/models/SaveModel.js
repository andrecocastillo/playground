"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveUserModel = void 0;
var SaveUserModel = /** @class */ (function () {
    function SaveUserModel(request_data) {
        this.name = '';
        this.email = '';
        this.password = '';
        this.typeUser = 0;
        this.name = request_data.name;
        this.email = request_data.email;
        this.password = request_data.password;
        this.typeUser = request_data.typeUser;
    }
    SaveUserModel.prototype.get_name = function () {
        return this.name;
    };
    SaveUserModel.prototype.set_name = function (value) {
        this.name = value;
    };
    SaveUserModel.prototype.get_email = function () {
        return this.email;
    };
    SaveUserModel.prototype.set_email = function (value) {
        this.name = value;
    };
    SaveUserModel.prototype.get_password = function () {
        return this.password;
    };
    SaveUserModel.prototype.set_password = function (value) {
        this.password = value;
    };
    SaveUserModel.prototype.get_typeUser = function () {
        return this.typeUser;
    };
    SaveUserModel.prototype.set_typeUser = function (value) {
        this.typeUser = value;
    };
    return SaveUserModel;
}());
exports.SaveUserModel = SaveUserModel;
exports.default = SaveUserModel;
