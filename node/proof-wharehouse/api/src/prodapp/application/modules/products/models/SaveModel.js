"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveModel = void 0;
var SaveModel = /** @class */ (function () {
    function SaveModel(request_data) {
        this.name = '';
        this.name = request_data.name;
        this.quantity = request_data.quantity;
        this.companyId = request_data.companyId;
    }
    SaveModel.prototype.get_name = function () {
        return this.name;
    };
    SaveModel.prototype.set_name = function (value) {
        this.name = value;
    };
    SaveModel.prototype.get_quantity = function () {
        return this.quantity;
    };
    SaveModel.prototype.set_quantity = function (value) {
        this.quantity = value;
    };
    SaveModel.prototype.get_companyId = function () {
        return this.companyId;
    };
    SaveModel.prototype.set_companyId = function (value) {
        this.companyId = value;
    };
    return SaveModel;
}());
exports.SaveModel = SaveModel;
exports.default = SaveModel;
