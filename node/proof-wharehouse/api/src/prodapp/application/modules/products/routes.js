"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var CompanyController_1 = __importDefault(require("./controllers/CompanyController"));
var GeneralController_1 = __importDefault(require("./controllers/GeneralController"));
var router = (0, express_1.Router)();
var general_controller = new GeneralController_1.default();
var company_controller = new CompanyController_1.default();
router.post('/register', general_controller.registerProduct);
router.put('/update', general_controller.updateProduct);
router.get('/list', general_controller.listProduct);
router.delete('/delete/:productId', general_controller.deleteProduct);
router.get('/get-information/:productId', general_controller.getInformation);
router.get('/generate-pdf-company/:companyId', company_controller.generatePdf);
exports.default = router;
