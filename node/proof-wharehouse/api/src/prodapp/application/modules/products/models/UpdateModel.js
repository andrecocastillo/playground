"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateModel = void 0;
var UpdateModel = /** @class */ (function () {
    function UpdateModel(request_data) {
        this.name = '';
        this.id = request_data.id;
        this.name = request_data.name;
        this.quantity = request_data.quantity;
        this.companyId = request_data.companyId;
        this.created = request_data.created;
    }
    UpdateModel.prototype.get_id = function () {
        return this.id;
    };
    UpdateModel.prototype.set_id = function (value) {
        this.id = value;
    };
    UpdateModel.prototype.get_name = function () {
        return this.name;
    };
    UpdateModel.prototype.set_name = function (value) {
        this.name = value;
    };
    UpdateModel.prototype.get_quantity = function () {
        return this.quantity;
    };
    UpdateModel.prototype.set_quantity = function (value) {
        this.quantity = value;
    };
    UpdateModel.prototype.get_companyId = function () {
        return this.companyId;
    };
    UpdateModel.prototype.set_companyId = function (value) {
        this.companyId = value;
    };
    UpdateModel.prototype.get_created = function () {
        return this.created;
    };
    UpdateModel.prototype.set_created = function (value) {
        this.created = value;
    };
    return UpdateModel;
}());
exports.UpdateModel = UpdateModel;
exports.default = UpdateModel;
