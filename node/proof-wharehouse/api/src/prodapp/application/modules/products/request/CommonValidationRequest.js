"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonValidationRequest = /** @class */ (function () {
    function CommonValidationRequest() {
        this.standar_message = 'Input data error.';
    }
    return CommonValidationRequest;
}());
exports.default = CommonValidationRequest;
