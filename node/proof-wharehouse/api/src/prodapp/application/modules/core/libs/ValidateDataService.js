"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validate_typescript_1 = require("validate-typescript");
var ValidateDataService = /** @class */ (function () {
    function ValidateDataService(rules_validation, object_entity) {
        this.rules_validation = rules_validation;
        this.object_entity = object_entity;
    }
    ValidateDataService.prototype.getResults = function () {
        try {
            (0, validate_typescript_1.validate)(this.rules_validation, this.object_entity);
            return { result: true };
        }
        catch (e) {
            console.log('Error:', e);
            return { result: false };
        }
    };
    return ValidateDataService;
}());
exports.default = ValidateDataService;
