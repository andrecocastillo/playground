"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt = __importStar(require("jsonwebtoken"));
var Application_1 = __importDefault(require("../constants/Application"));
var Headers_1 = __importDefault(require("../constants/Headers"));
var Jwtsing = /** @class */ (function () {
    function Jwtsing() {
    }
    Jwtsing.prototype.generateSing = function (user_id) {
        var token_string = jwt.sign({ user_id: user_id, type: 'USER' }, Application_1.default.jwtSecret, { expiresIn: "6h" });
        return token_string;
    };
    Jwtsing.prototype.getDataToken = function () {
        var information = jwt.decode(Headers_1.default.autorization);
        return information;
    };
    return Jwtsing;
}());
exports.default = Jwtsing;
