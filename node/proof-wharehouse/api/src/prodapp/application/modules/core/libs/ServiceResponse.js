"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_status_codes_1 = require("http-status-codes");
var ServiceResponse = /** @class */ (function () {
    function ServiceResponse() {
        this.response_status = false;
        this.response_message = '';
    }
    ServiceResponse.prototype.createSussesProcces = function (response_data_object, response_message) {
        this.response_status = true;
        this.status_code = http_status_codes_1.StatusCodes.OK;
        this.response_data_object = response_data_object;
        this.response_message = response_message;
    };
    ServiceResponse.prototype.createErrorProcces = function (response_data_object, response_message) {
        this.response_status = false;
        this.status_code = http_status_codes_1.StatusCodes.BAD_REQUEST;
        this.response_data_object = response_data_object;
        this.response_message = response_message;
        throw new Error(response_message);
    };
    ServiceResponse.prototype.setRequestError = function (error) {
        this.response_status = false;
        this.status_code = http_status_codes_1.StatusCodes.BAD_REQUEST;
        this.response_data_object = {};
        this.response_message = error.message;
    };
    ServiceResponse.prototype.getResponseDataObject = function () {
        return this.response_data_object;
    };
    ServiceResponse.prototype.response = function (res) {
        return res.status(http_status_codes_1.StatusCodes.OK).json({
            res: this.response_status,
            dataObj: this.response_data_object,
            msg: this.response_message
        });
    };
    return ServiceResponse;
}());
exports.default = ServiceResponse;
