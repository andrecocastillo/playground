"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkJwt = exports.headers = void 0;
var http_status_codes_1 = require("http-status-codes");
var jwt = __importStar(require("jsonwebtoken"));
var Application_1 = __importDefault(require("../constants/Application"));
var Headers_1 = __importDefault(require("../constants/Headers"));
var headers = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    next();
};
exports.headers = headers;
var checkJwt = function (req, res, next) {
    // Set headers data to global information
    if (req.url.indexOf('/login/access') != -1) {
        next();
    }
    else {
        Headers_1.default.autorization = String(req.headers.authorization);
        // console.log( req.url.indexOf('/core/logs') );
        // if(
        //     req.url.indexOf('/core/logs') != -1 
        // ){
        //     next();
        // }
        try {
            jwt.verify(Headers_1.default.autorization, Application_1.default.jwtSecret);
            var token_information = jwt.decode(Headers_1.default.autorization);
            if ((token_information.type == 'USER')) {
                next();
            }
            else {
                throw new Error('Wrong token');
            }
        }
        catch (err) {
            console.log(err);
            return res.status(http_status_codes_1.StatusCodes.CONFLICT).json({
                res: false,
                dataObj: [req.url, err],
                msg: "Wrong token"
            });
        }
    }
};
exports.checkJwt = checkJwt;
