"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var GeneralController_1 = __importDefault(require("./controllers/GeneralController"));
var router = (0, express_1.Router)();
var general_controller = new GeneralController_1.default();
router.get('/version', general_controller.version);
router.get('/user-test', general_controller.userTest);
exports.default = router;
