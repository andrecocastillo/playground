"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyModel = void 0;
var CompanyModel = /** @class */ (function () {
    function CompanyModel(request_data) {
        this.name = '';
        this.address = '';
        this.company_id = request_data.company_id;
        this.name = request_data.name;
        this.address = request_data.address;
        this.nit = request_data.nit;
        this.phone = request_data.phone;
    }
    CompanyModel.prototype.get_company_id = function () {
        return this.company_id;
    };
    CompanyModel.prototype.set_company_id = function (value) {
        this.company_id = value;
    };
    CompanyModel.prototype.get_id = function () {
        return this.company_id;
    };
    CompanyModel.prototype.set_id = function (value) {
        this.company_id = value;
    };
    CompanyModel.prototype.get_name = function () {
        return this.name;
    };
    CompanyModel.prototype.set_name = function (value) {
        this.name = value;
    };
    CompanyModel.prototype.get_address = function () {
        return this.address;
    };
    CompanyModel.prototype.set_address = function (value) {
        this.address = value;
    };
    CompanyModel.prototype.get_nit = function () {
        return this.nit;
    };
    CompanyModel.prototype.set_nit = function (value) {
        this.nit = value;
    };
    CompanyModel.prototype.get_phone = function () {
        return this.phone;
    };
    CompanyModel.prototype.set_phone = function (value) {
        this.phone = value;
    };
    return CompanyModel;
}());
exports.CompanyModel = CompanyModel;
exports.default = CompanyModel;
