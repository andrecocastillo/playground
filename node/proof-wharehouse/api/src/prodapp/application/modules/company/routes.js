"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var CompanyController_1 = __importDefault(require("./controllers/CompanyController"));
var router = (0, express_1.Router)();
var company_controller = new CompanyController_1.default();
router.post('/save', company_controller.saveCompany);
router.get('/list', company_controller.listCompanies);
router.get('/get-information/:company_id', company_controller.informationCompany);
router.delete('/delete/:company_id', company_controller.deleteCompany);
router.put('/update', company_controller.updateCompany);
exports.default = router;
