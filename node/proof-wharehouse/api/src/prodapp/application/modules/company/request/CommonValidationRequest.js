"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonValidationRequest = /** @class */ (function () {
    function CommonValidationRequest() {
        this.standar_message = 'All fields are required.';
    }
    return CommonValidationRequest;
}());
exports.default = CommonValidationRequest;
