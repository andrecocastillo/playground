import axios from 'axios';

const API_URL = 'http://localhost:3500/';

export class Rest{

    mockupEnabled:boolean = false;
    token:string = '';

    constructor(){
        this.mockupEnabled = false;
        this.token = localStorage.getItem('TOKEN') ?? "";
    }

    post(url:string, data:any){
      this.mockupEnabled = false;
      return this.call('POST', url, data);
    } 
    
    put(url:string, data:any){
      this.mockupEnabled = false;
      return this.call('PUT', url, data);
    } 
    
    get(url:string){
      this.mockupEnabled = false;
      return this.call('GET', url);
    }  

    call(method:string, url:string, data:any= {}, response:any = {}){
      let headers = {
        "content-type": 'application/json',
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Authorization": this.token
      };

      data.jsonstring = response;

      const config = {
        method: method,
        url: API_URL + url,
        data: data,
        headers: headers,
      };

      return axios(config);
    }

    mockup(method:string, url:string, data:any, response:any){
      return this.call(method, url, data, response);
    }

    postMockup(url:string, data:any, response:any = {}){
      this.mockupEnabled = true;
      return this.mockup('POST', url, data, response);
    }

    getMockup(url:string, response:any = {}){
      this.mockupEnabled = true;
      return this.mockup('POST', url, {}, response);
    }
}