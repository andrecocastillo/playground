import {Button, Container, Nav, Navbar as NavbarBs} from "react-bootstrap"
import { NavLink } from "react-router-dom";

function Navbar() {

  return (
    <NavbarBs sticky="top" className="bg-white shadow-sm my-0"> 
      <Container> 
        <Nav className="me-auto">
          <Nav.Link to ="/home/welcome" as={NavLink}> 
            Home 
          </Nav.Link>
          <Nav.Link to ="/users/list" as={NavLink}> 
            Users 
          </Nav.Link>
          <Nav.Link to ="/company/list" as={NavLink}> 
            Companies
          </Nav.Link>
          <Nav.Link to ="/products/list" as={NavLink}> 
            Products
          </Nav.Link>
          <Nav.Link to ="/" as={NavLink}> 
            Exit
          </Nav.Link>
        </Nav>
      </Container>
    </NavbarBs>
  );
}

export default Navbar;
