import React from 'react';
import './App.css';
import { Routes, Route } from "react-router-dom"
import Login from './pages/login/Login';
import NewCompany from './pages/companies/NewCompany';
import Welcome from './pages/home/Welcome';
import CreateUser from './pages/users/CreateUser';
import ListCompanies from './pages/companies/ListCompanies';
import ListProducts from './pages/products/ListProducts';
import NewProduct from './pages/products/NewProduct';
import ListUsers from './pages/users/ListUsers';
import EditUser from './pages/users/EditUser';
import EditCompany from './pages/companies/EditCompany';
import EditProduct from './pages/products/EditProduct';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home/welcome" element={<Welcome />} />
        <Route path="/users/new" element={<CreateUser />} />
        <Route path="/users/edit/:userId" element={<EditUser />} />
        <Route path="/users/list" element={<ListUsers />} />
        <Route path="/company/new" element={<NewCompany />} />
        <Route path="/company/edit/:companyId" element={<EditCompany />} />
        <Route path="/company/list" element={<ListCompanies />} />
        <Route path="/products/list" element={<ListProducts />} />
        <Route path="/products/new" element={<NewProduct />} />
        <Route path="/products/edit/:productId" element={<EditProduct />} />
      </Routes>
    </div>
  );
}

export default App;