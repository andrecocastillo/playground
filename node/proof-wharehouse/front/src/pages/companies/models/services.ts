export type Company = {
    name: string
    address: string
    nit: string
    phone: number
};

type CompanyId = {
    companyId: number
};

type CompanyIdWs = {
    company_id: number
};



export type EditCompany =  Company & CompanyId;
export type CompanyFullData =  Company & CompanyId;
export type CompanyWs =  Company & CompanyIdWs;