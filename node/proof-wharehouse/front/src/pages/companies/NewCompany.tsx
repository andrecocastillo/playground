import Navbar from "../../core/componets/Navbar";
import FormCompany from "./components/FormCompany";

export default function NewCompany() {
  return (
    <div id="view-company-new">
      <Navbar />
      
      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Companies</li>
            <li>Create company</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <FormCompany />
      </div>
    </div>
  );
}