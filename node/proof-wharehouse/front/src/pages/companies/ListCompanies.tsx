import { useNavigate } from "react-router-dom";
import Navbar from "../../core/componets/Navbar";
import TableCompanies from "./components/TableCompanies";

export default function ListCompanies() {
  const navigate = useNavigate();
    
  const createCompany = () => {
    navigate("/company/new");
  };

  return (
    <div id="view-company-list">
      <Navbar />

      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Comanies</li>
            <li>List companies</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <div className="panel-buttons d-flex justify-content-end mb-4">
          <button onClick={createCompany} className="btn btn-outline-success">Nuevo</button>
        </div>

        <TableCompanies />
      </div>
    </div>
  );
}