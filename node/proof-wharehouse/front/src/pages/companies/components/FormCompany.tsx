import { useEffect } from "react";
import { Alert, Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Company } from "../models/services";
import CompanyService from "../services/CompanyService";

type FormCompanyProps = {
  companyId?: string
}

export default function FormCompany({companyId}:FormCompanyProps) {
  const navigate = useNavigate();
  const {register, formState: { errors }, handleSubmit, setValue} = useForm<Company>();

  const saveDataCompany = handleSubmit((data)=>{
    if(companyId){
      CompanyService.editCompany({
        companyId:parseInt(companyId ?? ''),
        name:data.name,
        address:data.address,
        nit:data.nit,
        phone:data.phone
      }).then( 
        response => {
          navigate('/company/list');
        },
        error => {}
      );
    } else {
      CompanyService.registerCompany(data).then(
        response => {
          navigate('/company/list');
        },
        error => {}
      );
    }
  });

  useEffect(() => {
    if(companyId){
      CompanyService.getInformationCompany(parseInt(''+companyId)).then( 
        response => {
          setValue('name', response.data.dataObj.name);
          setValue('address', response.data.dataObj.address);
          setValue('nit', response.data.dataObj.nit);
          setValue('phone', response.data.dataObj.phone);
        },
        error => {}
      );
    }
  }, []);

  return (
    <div id="component-form-company" className="custom-card">
      <div className="card-title py-3 px-3 px-sm-4">
        <h3 className="my-0 fz-16 fw-700 primary--text">Manage company</h3>
      </div>
      <div className="card-body p-3 p-sm-4 p-md-5">
        <Form id="form-login" onSubmit={saveDataCompany}>
          <div className="row">
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control {...register("name", { required: true })} type="text" placeholder="Email de su acceso" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Addres</Form.Label>
                <Form.Control {...register("address", { required: true })} type="text" placeholder="Ingrese direccion de la oficina" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Nit</Form.Label>
                <Form.Control {...register("nit", { required: true })} type="text" placeholder="Ingrese NIT" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Phone</Form.Label>
                <Form.Control {...register("phone", { required: true })} type="text" placeholder="Ingrese numero celular" />
              </Form.Group>
            </div>
          </div>

          {(Object.keys(errors).length > 0)  && 
              <Alert variant="warning">
                <ul>
                  {errors.name && <li>Nombre requerido</li>}
                  {errors.address && <li>Direccion requerido </li>}
                  {errors.nit && <li>Nit requerida </li>}
                  {errors.phone && <li>Telefono requerida </li>}
                </ul>
              </Alert>
            }

          <Form.Group className="my-0" controlId="formBasicEmail">
            <Button variant="primary" className="btn btn-primary d-table mx-auto" type="submit">Save</Button>
          </Form.Group>
        </Form>
      </div>
    </div>
  );
}