import { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router-dom";
import { CompanyWs } from "../models/services";
import CompanyService from "../services/CompanyService";

type Company = {
  id:number
  name:string
  phone:string
}

export default function TableCompanies() {
  const navigate = useNavigate();
  const [companies, setCompanies] = useState<Company[]>([]);


  const columns:Array<any> = [
    {
      name: "Id",
      selector: "company_id",
      sortable: true
    },
    {
      name: "name",
      selector: "name",
      sortable: false
    },
    {
      name: "Agrees",
      selector: "address",
      sortable: false
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 100,
      align: "left",
      sortable: false,
      cell: (record:CompanyWs) => {
        return (
          <div>
            <button className="btn btn-primary btn-sm" onClick={() => { navigate('/company/edit/'+record.company_id); }}>Edit</button>
          </div>
        );
      }
    }
  ];
    
  useEffect(() => {
    CompanyService.allCompanies().then(
      response => {
        if(response.data.dataObj){
          setCompanies(response.data.dataObj);
        }
      },
      error => {
      })
  }, []);

  return (
    <div id="component-table-companies" className="custom-card">
      <div className="card-title py-3 px-3 px-sm-4">
        <h3 className="my-0 fz-16 fw-700 primary--text">List companies</h3>
      </div>
      <div className="card-body">
        <DataTable
          columns={columns}
          data={companies}
          pagination
          selectableRows
        />
      </div>
    </div>
  );
}