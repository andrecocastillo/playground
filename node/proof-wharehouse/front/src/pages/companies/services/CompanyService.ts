import { Rest } from "../../../core/libs/Rest";
import { Company, EditCompany } from "../models/services";

class CompanyService {

    rest:Rest = new Rest();

    allCompanies() {       
      return this.rest.get('company/list');
    }

    getInformationCompany(company_id:number) {       
      return this.rest.get('company/get-information/'+company_id);
    }

    editCompany(dataCompany:EditCompany) {
      let data = {
        "company_id": dataCompany.companyId,
        "name": dataCompany.name,
        "address": dataCompany.address,
        "nit": dataCompany.nit,
        "phone": dataCompany.phone
    }
      return this.rest.put('company/update', data);
    }

    registerCompany(dataCompany:Company) {
      let data = {
        "name": dataCompany.name,
        "address": dataCompany.address,
        "nit": dataCompany.nit,
        "phone": dataCompany.phone
      };

      return this.rest.post('company/save', data);
    }
}


export default new CompanyService();