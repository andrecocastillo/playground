import Navbar from "../../core/componets/Navbar";

export default function Welcome() {
    
    return (
      <div id="view-home">
        <Navbar />

        <div className="breadcrumb py-3">
          <div className="container">
            <ul className="list-unstyled m-0 p-0 step-list">
              <li>Dashboard</li>
            </ul>
          </div>
        </div>

        <div className="container py-4 py-sm-5">
          <h1>Welcome</h1>
        </div>
      </div>
    );
}