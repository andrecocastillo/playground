import { Rest } from "../../../core/libs/Rest";
import { EditProduct, Product } from "../models/services";

class ProductsService {

    rest:Rest = new Rest();

    allProducts() {       
      return this.rest.get('products/list');
    }

    getInformationProduct(company:number) {
      return this.rest.getMockup('products/register/'+company);
    }

    editProduct(dataCompany:EditProduct) {       
      let request = {
        "id": dataCompany.productId,
        "name": dataCompany.name,
        "quantity": dataCompany.quantity,
        "companyId": dataCompany.companyId
      };
      return this.rest.put('products/update', request);
    }

    saveProduct(dataCompany:Product) {
      let request = {
        "name": dataCompany.name,
        "quantity": dataCompany.quantity,
        "companyId": dataCompany.companyId
      };
       
      return this.rest.post('products/register', request);
    }
}


export default new ProductsService();