import Navbar from "../../core/componets/Navbar";
import FromCrudProduct from "./componets/FromCrudProduct";

export default function NewProduct() {
  return (
    <div id="view-producst-new">
      <Navbar />

      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Products</li>
            <li>Create product</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <FromCrudProduct />
      </div>
    </div>
  );
}