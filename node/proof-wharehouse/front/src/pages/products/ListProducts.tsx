import { useNavigate } from "react-router-dom";
import Navbar from "../../core/componets/Navbar";
import TableProducts from "./componets/TableProducts";

export default function ListProducts() {
  const navigate = useNavigate();
  
  const createProduct = () => {
    navigate("/products/new");
  };

  return (
    <div id="view-producst-list">
      <Navbar />

      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Products</li>
            <li>List products</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <div className="panel-buttons d-flex justify-content-end mb-4">
          <button onClick={createProduct} className="btn btn-outline-success">Nuevo</button>
        </div>
      
        <TableProducts />
      </div>
    </div>
  );
}