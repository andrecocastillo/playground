export type Product = {
    companyId: boolean
    name: string
    quantity: string
}


type ProductId = {
    productId: number
};

type ProductIdWs = {
    product_id: number
};

export type EditProduct =  Product & ProductId;
export type ProducWs =  Product & ProductIdWs;