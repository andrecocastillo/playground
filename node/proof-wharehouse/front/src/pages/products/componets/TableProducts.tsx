import { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router-dom";
import { ProducWs } from "../models/services";
import ProductsService from "../services/ProductsService";

type Company = {
  id:number
  name:string
  phone:string
}

export default function TableProducts() {
  const navigate = useNavigate();
  const [companies, setCompanies] = useState<Company[]>([]);

  const columns:Array<any> = [
    {
      name: "Id",
      selector: "product_id",
      sortable: true
    },
    {
      name: "name",
      selector: "name",
      sortable: false
    },
    {
      name: "Cantidad",
      selector: "quantity",
      sortable: false
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 100,
      align: "left",
      sortable: false,
      cell: (record:ProducWs) => {
        return (
          <div>
            <button className="btn btn-primary btn-sm" onClick={() => { navigate('/products/edit/'+record.product_id); }}>Edit</button>
          </div>
        );
      }
    }
  ];
    
  useEffect(() => {
    ProductsService.allProducts().then(
    response => {
      if(response.data.dataObj){
        setCompanies(response.data.dataObj);
      }
    },
    error => {
    })
  }, []);

  return (
    <div id="component-table-products" className="custom-card">
      <div className="card-title py-3 px-3 px-sm-4">
        <h3 className="my-0 fz-16 fw-700 primary--text">List products</h3>
      </div>
      <div className="card-body">
        <DataTable
          columns={columns}
          data={companies}
          pagination
          selectableRows
        />
      </div>
    </div>
  );
}