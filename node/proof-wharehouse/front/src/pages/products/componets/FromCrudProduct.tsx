import { useEffect, useState } from "react";
import { Alert, Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Company, CompanyFullData } from "../../companies/models/services";
import CompanyService from "../../companies/services/CompanyService";
import { Product } from "../models/services";
import ProductsService from "../services/ProductsService";

type FromCrudProductProps = {
  productIdEdit?: string
}

export default function FromCrudProduct({productIdEdit}:FromCrudProductProps) {
  const navigate = useNavigate();
  const {register, formState: { errors }, handleSubmit, setValue} = useForm<Product>();
  const [companies, setCompanies] = useState<CompanyFullData[]>([]);

  const saveProduct = handleSubmit(
    (data) => {
      if(productIdEdit){
        let productId = parseInt(productIdEdit ?? '');
        ProductsService.editProduct({...data, productId}).then( 
          response => {
            navigate('/products/list');
          },
          error => {}
        );
      }else{
        ProductsService.saveProduct(data).then( 
          response => {
            navigate('/products/list');
          },
          error => {}
        );
      }
    }
  );

  useEffect(() => {
    if(productIdEdit){
      ProductsService.getInformationProduct(parseInt(''+productIdEdit)).then( 
        response => {
          setValue('companyId', response.data.dataObj.companyId);
          setValue('name', response.data.dataObj.name);
          setValue('quantity', response.data.dataObj.quantity);
        },
        error => {}
      );
    }

    CompanyService.allCompanies().then( 
      response => {
        setCompanies(response.data.dataObj);
      },
      error => {}
    );
    }, []);

    return (
      <div id="component-form-products" className="custom-card">
        <div className="card-title py-3 px-3 px-sm-4">
          <h3 className="my-0 fz-16 fw-700 primary--text">Manage product</h3>
        </div>
        <div className="card-body p-3 p-sm-4 p-md-5">
          <Form id="form-login" onSubmit={saveProduct}>
            <div className="row">
              <div className="col-sm-6 col-lg-4 mb-3 mb-md-4">
                <Form.Group className="my-0" controlId="formBasicEmail">
                  <Form.Label>Empresa</Form.Label>
                  <Form.Select {...register("companyId", { required: true })} aria-label="Default select example" className="form-control">
                  {companies.map((companyInfo , index) => (
                    <option key={index} value={companyInfo.companyId}>{companyInfo.name}</option>
                  ))}
                  </Form.Select>
                </Form.Group>
              </div>
              <div className="col-sm-6 col-lg-4 mb-3 mb-md-4">
                <Form.Group className="my-0" controlId="formBasicEmail">
                  <Form.Label>Name</Form.Label>
                  <Form.Control {...register("name", { required: true })} type="text" placeholder="Nombre del producto" />
                </Form.Group>
              </div>
              <div className="col-sm-6 col-lg-4 mb-3 mb-md-4">
                <Form.Group className="my-0" controlId="formBasicEmail">
                  <Form.Label>Cantidad</Form.Label>
                  <Form.Control {...register("quantity", { required: true })} type="number" placeholder="Cantidad de unidades disponible" />
                </Form.Group>
              </div>
            </div>

            {(Object.keys(errors).length > 0)  && 
            <ul>
              <Alert variant="warning">
                {errors.companyId && <li>Compañia requerida </li>}
                {errors.name && <li>Nombre requerida </li>}
                {errors.quantity && <li>Cantidad requerida </li>}
              </Alert>
            </ul>
            }

            <Form.Group className="my-0" controlId="formBasicEmail">
              <Button variant="primary" type="submit" className="btn btn-primary d-table mx-auto">Save</Button>
            </Form.Group>
          </Form>
        </div>
      </div>
    );
}