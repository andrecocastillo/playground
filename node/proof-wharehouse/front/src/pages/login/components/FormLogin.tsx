import { Alert, Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { useForm } from "react-hook-form";
import { Navigate, useNavigate } from "react-router-dom";

import LoginService from '../services/LoginService';

type Login = {
  username: string
  password: string
}

export default function FormLogin() {
  const navigate = useNavigate();
  const {register, formState: { errors }, setError, handleSubmit} = useForm<Login>();
  
  const loginUser = handleSubmit((data) => {
    LoginService.accessUser(data.username, data.password).then(
      response => {
        if(response.data.dataObj.tokenString){
          localStorage.setItem('TOKEN', response.data.dataObj.tokenString);
          navigate("home/welcome");
        }else{
          setError('root.userNotExist', { 
            type: 'custom', message: 'El usuario no existe en el sistema'
          });
        }
      },
      error => {}
    );
  });

  
  return (
    <div id="component-form-login" className="card p-3 p-sm-4 p-lg-5 shadow border-0">
      <img src="https://belandmarcpetshop.com/wp-content/uploads/2023/02/cropped-logo-1.png" width="50%" className="d-block mx-auto mb-4" alt="" />

      <Form id="form-login" onSubmit={loginUser}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control {...register("username", { required: true })} type="email" placeholder="Email de su acceso" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Password</Form.Label>
          <Form.Control {...register("password", { required: true })} type="password" placeholder="contraseña" />
        </Form.Group>

        {(Object.keys(errors).length > 0)  && 
          <Alert variant="warning">
            <ul>
              {errors.username && <li>Email incorrecto</li>}
              {errors.password && <li>Password incorrecto</li>}
              {errors?.root?.userNotExist && <li>El usuario no existe</li>}
            </ul>
          </Alert>
        }
        <Form.Group className="mb-0" controlId="formBasicEmail">
        <Button variant="primary" type="submit" className="btn btn-primary d-table mx-auto">
          Submit
        </Button>
        </Form.Group>
      </Form>
    </div>
  );
}