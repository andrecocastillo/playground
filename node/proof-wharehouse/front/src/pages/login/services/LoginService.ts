import { Rest } from "../../../core/libs/Rest";

class LoginService {

    rest:Rest = new Rest();

    accessUser(username:string, password:string) {
      return this.rest.post('login/access', {username, password});
    }
}


export default new LoginService();