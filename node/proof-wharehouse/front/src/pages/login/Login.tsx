import Navbar from "../../core/componets/Navbar";
import FormLogin from "./components/FormLogin"

type Login = {
  userEmail: string
  password: string
}

export default function Login() {
    return (
      <div id="view-login" className="view-all-screen" style={{ backgroundImage: 'url(https://cdn.pixabay.com/photo/2016/11/29/09/16/architecture-1868667_960_720.jpg)'}}>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-sm-10 col-md-8 col-lg-6 col-xl-4">
              <FormLogin />
            </div>
          </div>
        </div>
    </div>
    );
}