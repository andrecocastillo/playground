import { useParams } from 'react-router-dom';
import Navbar from '../../core/componets/Navbar';
import FormUser from './components/FormUser'

export default function EditUser() {
  let { userId } = useParams();
  
  return (
    <div id="view-users-edit">
      <Navbar />

      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Users</li>
            <li>Edit user</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <FormUser userIdEdit={userId} />
      </div>
    </div>
  );
}