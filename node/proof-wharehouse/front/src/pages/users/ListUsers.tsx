import { useNavigate } from "react-router-dom";
import Navbar from "../../core/componets/Navbar";
import TableUsers from "./components/TableUsers";


export default function ListUsers() {
  const navigate = useNavigate();
  const createUser = () => {
    navigate("/users/new");
  };
  
  return (
    <div id="view-user-list">
      <Navbar />

      <div className="breadcrumb py-3">
        <div className="container">
          <ul className="list-unstyled m-0 p-0 step-list">
            <li>Project</li>
            <li>Users</li>
            <li>List users</li>
          </ul>
        </div>
      </div>

      <div className="container py-4 py-sm-5">
        <div className="panel-buttons d-flex justify-content-end mb-4">
          <button onClick={createUser} className="btn btn-outline-success">Nuevo</button>
        </div>

        <TableUsers />
      </div>
    </div>
  );
}