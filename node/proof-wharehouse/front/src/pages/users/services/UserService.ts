import { Rest } from "../../../core/libs/Rest";
import { EditUser, User } from "../models/services";

class UserService {

    rest:Rest = new Rest();

    createUser(dataUser:User) {
      let request = {
        "name":dataUser.name,
        "email": dataUser.email,
        "password":dataUser.password,
        "typeUser":dataUser.type
      };
      
      return this.rest.post('users/register', request);
    }

    allUsers(){
      return this.rest.get('users/list');      
    }

    getInformationUser(userId:string) {       
      return this.rest.get('users/get-information/'+userId);
    }

    edituser(dataUser:EditUser) {
      let request = {
        "id": dataUser.userId,
        "name": dataUser.name,
        "email": dataUser.email,
        "password": dataUser.password,
        "typeUser": dataUser.type
    };
       
      return this.rest.put('users/update', request);
    }
}


export default new UserService();