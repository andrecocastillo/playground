import { useState, useEffect } from "react";
import { Alert, Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { User } from "../models/services";
import UserService from "../services/UserService";

type FormUserProps = {
  userIdEdit?: string
}

export default function FormUser({userIdEdit}:FormUserProps) {
  const navigate = useNavigate();
  const {register, formState: { errors }, handleSubmit, setValue} = useForm<User>();


  const saveDataUser = handleSubmit(
    (data) => {
      if(userIdEdit){
        let userId = parseInt(userIdEdit ?? '');
        UserService.edituser({...data, userId}).then(
          response => {
            navigate('/users/list');
          },
          error => {
        });
      }else{
        UserService.createUser(data).then(
          response => {
            navigate('/users/list');
          },
          error => {
        });
      }
  });
    
  useEffect(() => {
    if(userIdEdit){
      UserService.getInformationUser(userIdEdit ?? '').then( 
        response => {
          setValue('name', response.data.dataObj.name);
          setValue('email', response.data.dataObj.email);
          setValue('type', response.data.dataObj.type);
        },
        error => {}
      );
    }
  }, []);

  return (
    <div id="component-form-user" className="custom-card">
      <div className="card-title py-3 px-3 px-sm-4">
        <h3 className="my-0 fz-16 fw-700 primary--text">Manage user</h3>
      </div>
      <div className="card-body p-3 p-sm-4 p-md-5">
        <Form id="form-login" onSubmit={saveDataUser}>
          <div className="row">
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control {...register("name", { required: true })} type="text" placeholder="Su nombre" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control {...register("email", { required: true })} type="email" placeholder="Email corporativo" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Password</Form.Label>
                <Form.Control {...register("password", { required: true })} type="password" placeholder="contraseña" />
              </Form.Group>
            </div>
            <div className="col-sm-6 mb-3 mb-md-4">
              <Form.Group className="my-0" controlId="formBasicEmail">
                <Form.Label>Tipo de usuario</Form.Label>
                <Form.Select {...register("type", { required: true })} aria-label="Default select example" className="form-control">
                  <option value="1">Admin</option>
                  <option value="2">Cliente</option>
                </Form.Select>
              </Form.Group>
            </div>
          </div>

          {(Object.keys(errors).length > 0)  && 
            <Alert variant="warning">
              {errors.name && <span>Nombre incorrecto </span>}
              {errors.email && <span>Email incorrecto </span>}
              {errors.password && <span>Password incorrecto </span>}
              {errors.type && <span>Type incorrecto </span>}
            </Alert>
          }

          <Form.Group className="my-0" controlId="formBasicEmail">
            <Button variant="primary" className="btn btn-primary d-table mx-auto" type="submit">Save</Button>
          </Form.Group>
        </Form>
      </div>
    </div>
  );
}