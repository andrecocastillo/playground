import { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router-dom";
import { UserWebService } from "../models/services";
import UserService from "../services/UserService";

type User = {
  id:number
  name:string
  typeName:string
}

export default function TableUsers() {
  const navigate = useNavigate();
  const [listUsers, setListUsers] = useState<User[]>([]);

  const columns:Array<any> = [
    {
      name: "Id",
      selector: "user_id",
      sortable: true
    },
    {
      name: "name",
      selector: "name",
      sortable: false
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 100,
      align: "left",
      sortable: false,
      cell: (record:UserWebService) => {
        return (
          <div>
            <button className="btn btn-primary btn-sm" onClick={() => { navigate('/users/edit/'+record.user_id); }}>Edit</button>
          </div>
        );
      }
    }
  ];
    
  useEffect(() => {
    UserService.allUsers().then(
      response => {
        if(response.data.dataObj){
          setListUsers(response.data.dataObj);
        }
      },
      error => {
      })
  }, []);

  return (
    <div id="component-list-users" className="custom-card">
      <div className="card-title py-3 px-3 px-sm-4">
        <h3 className="my-0 fz-16 fw-700 primary--text">List users</h3>
      </div>
      <div className="card-body">
        <DataTable
          columns={columns}
          data={listUsers}
          pagination
          selectableRows
        />
      </div>
    </div>
  );
}