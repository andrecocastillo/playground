export type User = {
    name: string
    email: string
    password: string
    type: number
}

type UserId = {
    userId: number
};

type UserIdWs = {
    user_id: number
};

export type EditUser =  User & UserId;
export type UserWebService =  User & UserIdWs;