function helloWorldUser(name){
    return 'Hello '+ name +' !';
}


function firstLetterNameAndLastName(name, lastname){
    let letters = name.charAt(0) + lastname.charAt(0);
    return letters.toUpperCase();
}


function writeTextToDiv(idElement, text){
    const element = document.getElementById(idElement);
    element.innerText = text;
}

function simpleCaptcha(idElement){
    const elem = document.getElementById(idElement);
    let identifier = 'captcha-'+ Math.floor(Math.random() * 100);
    let numberOne = Math.floor(Math.random() * 100);
    let numberTwo = Math.floor(Math.random() * 100);
    let patternResult = (numberOne * numberTwo) - (numberOne - numberTwo);
    elem.innerHTML = '<div id="'+ identifier +'"> '+ numberOne +' + '+numberTwo+' = <input type="text" value=""> </div>';

    return [identifier, patternResult];
}

function pricePlusCostDelivery(cost){
    let total = cost + VALUE_DELIVERY;
    return CURRENCI_SYMBOL +' '+ total;
}