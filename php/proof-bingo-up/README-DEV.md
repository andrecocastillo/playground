# B I N G O
The project has 2 folders, backend and frontend.

# Back end

It is a Laravel project. To run the project use the command.

php artisan serve

This project will run at the URL http://127.0.0.1:8000

# front end

It is a react project. To run the project use the command.

npm run star

This project will run at http://localhost:3000/