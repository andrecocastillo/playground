-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_numbers` text NOT NULL,
  `date_register` datetime NOT NULL,
  `status` enum('O','C') NOT NULL DEFAULT 'O',
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `games` (`game_id`, `current_numbers`, `date_register`, `status`) VALUES
(2,	'[97,36,71,43,59,1,2,86,57]',	'2023-05-18 07:12:26',	'O');

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors` (
  `visitor_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `current_numbers` text NOT NULL,
  `board_numbers` text NOT NULL,
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`visitor_id`),
  KEY `game_id` (`game_id`),
  CONSTRAINT `visitors_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `visitors` (`visitor_id`, `game_id`, `name`, `current_numbers`, `board_numbers`, `date_register`) VALUES
(1,	2,	'andres',	'',	'[90,48,51,92,79,42,85,38,47,40,94,32,25,30,6,35,52,5,64,23,2,61,68,1,54]',	'2023-05-17 21:12:30'),
(2,	2,	'andres',	'',	'[1,45,33,79,23,56,64,62,37,43,20,89,48,85,19,34,8,92,49,50,14,99,70,9,24]',	'2023-05-17 21:12:48'),
(3,	2,	'Emma',	'[59]',	'[61,12,59,27,5,72,25,13,54,40,10,56,18,6,30,49,7,42,50,60,98,90,20,96,83]',	'2023-05-17 21:13:17'),
(4,	2,	'andres',	'',	'[32,81,87,73,79,13,83,85,91,48,38,46,18,77,65,15,58,93,29,52,99,4,98,50,57]',	'2023-05-17 21:17:42'),
(5,	2,	'Sam',	'[2,57]',	'[2,74,57,78,65,17,39,16,95,43,70,93,9,42,90,48,59,62,22,86,85,71,28,60,94]',	'2023-05-17 21:18:13');

-- 2023-05-17 21:21:51
