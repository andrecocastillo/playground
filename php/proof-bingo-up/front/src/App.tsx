import React from 'react';
import { Routes, Route } from "react-router-dom"
import Start from './pages/start/Start';
import Bingo from './pages/bingo/Bingo';
import "bootstrap/dist/css/bootstrap.min.css"
import './App.css';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Start />} />
        <Route path="bingo/game/:gameUserId" element={<Bingo />} />
      </Routes>
    </div>
  );
}

export default App;