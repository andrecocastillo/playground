import { Alert, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import Button from "react-bootstrap/esm/Button";
import { Navigate, useNavigate } from "react-router-dom";
import { Login } from "../models/login";
import StartGameService from "../services/StartGameService";

function FormStart() {
	const navigate = useNavigate();
  const {register, formState: { errors }, setError, handleSubmit} = useForm<Login>();

	const startBingo = handleSubmit((data) => {
    StartGameService.startGame(data).then(
      response => {
        if(response.data.data){
          localStorage.setItem('TOKEN', 'OIUYBhjkHKYYGYGhvVGVG');
					localStorage.setItem('USER_NAME', data.username);
					navigate("bingo/game/"+ response.data.data.visitor);
        }else{
					setError('root.unknown', { 
            type: 'custom', message: 'El usuario no existe en el sistema'
          });
				}
      },
      error => {}
    );
  });

	return (
		<div>
				<img src="https://belandmarcpetshop.com/wp-content/uploads/2023/02/cropped-logo-1.png" className="d-block mx-auto mb-4 image-login" alt="" />
				<Form id="form-login" onSubmit={startBingo}>
						{errors?.root?.unknown && <b>Unknown problem.</b>}
						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Your name</Form.Label>
							<Form.Control {...register("username", { required: true })} type="text" placeholder="Enter your name" />
						</Form.Group>
						<Form.Group className="mb-0" controlId="formBasicEmail">
						<Button variant="primary" type="submit" className="btn btn-primary d-table mx-auto">
							Generate Card
						</Button>
					</Form.Group>
				</Form>
		</div>
	);
}

export default FormStart;
