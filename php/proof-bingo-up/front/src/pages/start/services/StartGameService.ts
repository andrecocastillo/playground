import { Rest } from "../../../core/libs/Rest";
import { Login } from "../models/login";

class StartGameService {

    rest:Rest = new Rest();

    startGame(dataUser:Login) {
      let request = {
        "username":dataUser.username,
      };

      if(!dataUser.username){
        throw new Error('The user name is required');
      }
      
      return this.rest.post('visitor/register-visitor', request);
    }
}


export default new StartGameService();