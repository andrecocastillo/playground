import { Rest } from "../../../core/libs/Rest";
import { BingoBoard } from "../models/login";

const BING_POSITIONS = 25;

class BingoService {

    rest:Rest = new Rest();

    async generateBoard(boardId:number) {
      let datos = await this.rest.get('visitor/get-board/'+boardId);
      let boardNumber = JSON.parse(datos.data.data.board_numbers);
      let userSelection:any = datos.data.data.current_numbers ? JSON.parse(datos.data.data.current_numbers) : [];

      let listFields:Array<BingoBoard> = [];
      for(let position:number = 0; position < BING_POSITIONS; position++){  
        let value = boardNumber[position];
        let isSelected = (userSelection.find((element:number) => element == value)) ? true : false;

        listFields.push({position:position, value:value, isSelected:isSelected});
      }

      return [datos.data.data.game_id, listFields];
    }

    async saveUserSelection(gameUserId:number, valueData:number=0) {
      return await this.rest.put('visitor/save-visitor-selection/'+gameUserId, {
        selection:valueData
      });
    }

    async getNextNumber(gameId:number) {
      return await this.rest.get('game/get-next-number/'+gameId)
    }
}


export default new BingoService();