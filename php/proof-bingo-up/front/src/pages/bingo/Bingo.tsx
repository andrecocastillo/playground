import { useEffect, useState } from "react";
import Number from "./componets/Number";
import BingoService from "./services/BingoService";
import { BingoBoard } from "./models/login";
import StartGameService from "../start/services/StartGameService";
import { useNavigate, useParams } from "react-router-dom";
import General from "../../core/helpers/General";
import NextNumber from "./componets/NextNumber";
import { Alert } from "react-bootstrap";

function Bingo() {
    const [listFields, setListFields] = useState<BingoBoard[]>([]);
    const [gameId, setgameId] = useState<number>(0);
    const [nextNumber, setNextNumber] = useState<number>(0);
    const [messageBingo, setMessageBingo] = useState<string>("");

    const navigate = useNavigate();
    let { gameUserId } = useParams();

    useEffect(() => {
      setListFields([]);
      BingoService.generateBoard(General.toNumber(gameUserId)).then( 
        response => {
          setgameId(response[0]);
          setListFields(response[1]);
          setMessageBingo('');
        },
        error => {}
      );
    }, [gameUserId]);

    const newBoard = () =>{
      StartGameService.startGame({username: localStorage.getItem('USER_NAME') ?? ''}).then(
        response => {
          navigate("/bingo/game/"+ response.data.data.visitor);
        },
        error => {}
      );
    }

    return (
      <div className="bingo">
        <h2>GAME: {gameId} / REF: {gameUserId} </h2>
        <NextNumber nextNumber={nextNumber} setNextNumber={setNextNumber} gameId={General.toNumber(gameId)}></NextNumber>
        <div className="board">
          {listFields.map((data:BingoBoard) => (
            <Number 
              key={data.position} 
              gameUserId={General.toNumber(gameUserId)} 
              infoPosition={data}
              nextNumber={nextNumber}
              setMessageBingo={setMessageBingo}
            ></Number>
          ))}
        </div>
        <div className="panel">
          {messageBingo && <Alert variant="warning">{messageBingo}</Alert> }
          <button onClick={newBoard} className="btn btn-primary d-table mx-auto">New Board</button>
        </div>
      </div>
    );
  }
  
  export default Bingo;
  