import { useEffect, useState } from "react";
import { BingoBoard } from "../models/login";
import BingoService from "../services/BingoService";

type NextNumberProps = {
  gameId:number,
  nextNumber:any,
  setNextNumber:any
}

function NextNumber({gameId,nextNumber,setNextNumber}:NextNumberProps) {
  
    const getNewNumber = ()=>{
      BingoService.getNextNumber(gameId).then( 
        response => {
          setNextNumber(response.data.data);
        },
        error => {}
      );
    };

    return (
      <div className="next-number-cont">
            <b>Current Number: {nextNumber}</b>
            <button onClick={getNewNumber} >Next Number</button>
      </div>
    );
  }
  
  export default NextNumber;
  