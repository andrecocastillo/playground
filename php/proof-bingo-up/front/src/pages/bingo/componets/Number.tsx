import { useEffect, useState } from "react";
import { BingoBoard } from "../models/login";
import BingoService from "../services/BingoService";

type NumberProps = {
    gameUserId:number,
    infoPosition?: BingoBoard,
    nextNumber:any,
    setMessageBingo:any,
}

function Number({gameUserId,infoPosition,nextNumber,setMessageBingo}:NumberProps) {
  const [selected, setSelectedUser] = useState<boolean>();

    const numberSelected = () => {
      if(nextNumber == infoPosition?.value){
        BingoService.saveUserSelection(gameUserId, infoPosition?.value).then( 
          response => {},
          error => {}
        );
        setSelectedUser(true);
        setMessageBingo('');
      }else{
        setMessageBingo("The selected number does not match");
      }
    };

    let selectedUser = ((infoPosition?.isSelected || selected) ? 'isSelected' : '');
    return (
      <div onClick={numberSelected} className={"number " + selectedUser} >
            {infoPosition?.value}
      </div>
    );
  }
  
  export default Number;
  