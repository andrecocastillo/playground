export type BingoBoard = {
  position: number
  value: number
  isSelected ?: boolean
}