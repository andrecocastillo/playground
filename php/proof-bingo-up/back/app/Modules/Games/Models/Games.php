<?php

namespace App\Modules\Games\Models;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
  protected $table = 'games';
  protected $primaryKey = 'game_id';
  public $timestamps = false;
}
