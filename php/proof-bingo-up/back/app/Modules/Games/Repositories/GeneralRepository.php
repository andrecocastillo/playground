<?php

namespace App\Modules\Games\Repositories;

use App\Modules\Games\Models\Games;

class GeneralRepository
{
    public function selectEnabledGame(){
        return Games::where('status','O')
                ->first()
                ->toArray();
    }

    public function registerNewGame(){
        $gameModel = new Games();
        $gameModel['current_numbers'] = '';
        $gameModel['date_register'] = now();
        $gameModel['status'] = 'O';
        $gameModel->save();
    }

    public function getInformationGame($idGame){
        return Games::where('game_id',$idGame)
                ->first()
                ->toArray() ?? [];
    }

    public function updateCurentNumber($gameId, $currentNumbers){
        $flight = Games::find($gameId);
        $flight->current_numbers = json_encode($currentNumbers);
        $flight->save();
    }

    public function closeGame($gameId){
        $flight = Games::find($gameId);
        $flight->status = 'C';
        $flight->save();
    }

}