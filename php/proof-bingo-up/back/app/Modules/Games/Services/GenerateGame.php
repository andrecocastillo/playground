<?php
namespace App\Modules\Games\Services;

use App\Core\Libs\ServiceResponse\ServiceResponse;
use App\Modules\Games\Repositories\GeneralRepository;

class GenerateGame extends ServiceResponse
{
    protected $generalRepo;

    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    public function getCurrentGame()
    {
        $infoGame = $this->generalRepo->selectEnabledGame();
        if(empty($infoGame)){
            $this->generalRepo->registerNewGame();
            $infoGame = $this->generalRepo->selectEnabledGame();
        }

        $this->responseSuccess($infoGame, 'Current Game');
    }

    public function getNextNumber($request)
    {
        $infoGame = $this->generalRepo->getInformationGame($request->gameId);
        $numberSelected = 0;
        $currentNumbers = json_decode($infoGame['current_numbers']) ?? [];

        do {
            $numberSelected = rand(1, 99);
        } while (in_array($numberSelected, $currentNumbers) && count($currentNumbers) < 99);

        if(!$numberSelected){
            // Close Hame
            $this->generalRepo->closeGame($request->gameId);
        }else{
            $currentNumbers[] = $numberSelected;
            $this->generalRepo->updateCurentNumber($request->gameId, $currentNumbers);
        }

        $this->responseSuccess($numberSelected, 'Next number');
    }
}
