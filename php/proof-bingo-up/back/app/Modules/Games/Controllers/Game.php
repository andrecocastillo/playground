<?php
namespace App\Modules\Games\Controllers;

use App\Core\Exceptions\ResponseException;
use App\Http\Controllers\Controller;
use App\Modules\Games\Services\GenerateGame;
use Illuminate\Http\Request;

class Game extends Controller
{

	protected $gameService;

	public function __construct()
	{
			$this->gameService = new GenerateGame();
	}

	public function getNextNumber(Request $request)
	{
		try {
			$this->gameService->getNextNumber($request);
			return $this->gameService->HTTPresponse();
			} catch (ResponseException $e) {
					$this->gameService->responseError401($e->getMessage(), $e->getData());
					return $this->gameService->HTTPresponse();
			}
	}
}