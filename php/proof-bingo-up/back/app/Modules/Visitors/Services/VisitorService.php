<?php
namespace App\Modules\Visitors\Services;

use App\Core\Libs\ServiceResponse\ServiceResponse;
use App\Modules\Games\Services\GenerateGame;
use App\Modules\Visitors\Repositories\GeneralRepository;

class VisitorService extends ServiceResponse
{
    protected $generalCondition;

    public function __construct()
    {
        $this->generalCondition = new GeneralRepository();
    }

    public function registerVisitior($request)
    {
        // Information Game
        $GenerateGame = new GenerateGame();
        $GenerateGame->getCurrentGame();
        $gameInformation =  $GenerateGame->getData();

        $vistiorId = $this->generalCondition->registerVisitor($gameInformation['game_id'], $request->username, $this->generateNumbers());

        $this->responseSuccess(['visitor'=>$vistiorId, 'game_information'=>$gameInformation], 'Successful registration');
    }

    private function generateNumbers(){
        $numbers = range(1, 99);
        shuffle($numbers);
        return array_slice($numbers, 0, 25);
    }

    public function getBoard($request)
    {
        $boardInformation = $this->generalCondition->getBoardInformation($request->boardId);

        $this->responseSuccess($boardInformation, 'Board infomration');
    }

    public function saveVisitorSelector($request)
    {
        $boardInformation = $this->generalCondition->getBoardInformation($request->visitorId);
        $values = json_decode($boardInformation['current_numbers']);
        $selectionUser = (int) $request->selection;

        if(empty($values) || !in_array($selectionUser, $values)){
            $values[] = $selectionUser;
        }

        $this->generalCondition->updateSelectionVisitor($request->visitorId, $values);
        
        $this->responseSuccess([$boardInformation], 'Selection saved ');
    }
}
