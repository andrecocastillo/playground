<?php

namespace App\Modules\Visitors\Repositories;

use App\Modules\Visitors\Models\Visitors;

class GeneralRepository
{

    public function registerVisitor($gameId, $name, $numbers){
        $visitorModel = new Visitors();
        $visitorModel['game_id'] = $gameId;
        $visitorModel['name'] = $name;
        $visitorModel['current_numbers'] = '';
        $visitorModel['board_numbers'] = json_encode($numbers);
        $visitorModel['date_register'] = now();
        $visitorModel->save();

        return $visitorModel->visitor_id;
    }

    public function getBoardInformation($boardId){
        return Visitors::where('visitor_id', $boardId)
                ->first()
                ->toArray();
    }

    public function updateSelectionVisitor($visitorId, $values){
        $flight = Visitors::find($visitorId);
        $flight->current_numbers = json_encode($values);
        $flight->save();
    }
}