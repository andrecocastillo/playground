<?php

namespace App\Modules\Visitors\Request;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class RegisterVisitorRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $this->merge(['errors' => $validator->errors()]);
    }

    public function rules()
    {
        return [
            'username' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'username' => 'User name'
        ];
    }

    public function messages()
{
    return [
        'username.required' => ':attribute is requited.',
    ];
}
}
