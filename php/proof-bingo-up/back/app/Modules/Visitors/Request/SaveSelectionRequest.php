<?php

namespace App\Modules\Visitors\Request;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SaveSelectionRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $this->merge(['errors' => $validator->errors()]);
    }

    public function rules()
    {
        return [
            'selection' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'selection' => 'Selection'
        ];
    }

    public function messages()
{
    return [
        'selection.required' => ':attribute is requited.',
    ];
}
}
