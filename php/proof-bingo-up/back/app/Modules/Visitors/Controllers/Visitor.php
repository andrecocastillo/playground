<?php
namespace App\Modules\Visitors\Controllers;

use App\Core\Exceptions\ResponseException;
use App\Core\Helpers\ValidateRequest;
use App\Http\Controllers\Controller;
use App\Modules\Visitors\Request\RegisterVisitorRequest;
use App\Modules\Visitors\Request\SaveSelectionRequest;
use App\Modules\Visitors\Services\VisitorService;
use Illuminate\Http\Request;

class Visitor extends Controller
{

	protected $visitorService;

	public function __construct()
	{
			$this->visitorService = new VisitorService();
	}

	public function registerVisitor(RegisterVisitorRequest $request)
	{
		try {
			ValidateRequest::validateObject($request);
			
			$this->visitorService->registerVisitior($request);
			return $this->visitorService->HTTPresponse();
			} catch (ResponseException $e) {
					$this->visitorService->responseError401($e->getMessage(), $e->getData());
					return $this->visitorService->HTTPresponse();
			}
	} 
	
	public function getBoard(Request $request)
	{
		try {
			$this->visitorService->getBoard($request);
			return $this->visitorService->HTTPresponse();
			} catch (ResponseException $e) {
					$this->visitorService->responseError401($e->getMessage(), $e->getData());
					return $this->visitorService->HTTPresponse();
			}
	} 

	public function saveVisitorSelector(SaveSelectionRequest $request)
	{

		try {
			ValidateRequest::validateObject($request);
			
			$this->visitorService->saveVisitorSelector($request);
			return $this->visitorService->HTTPresponse();
			} catch (ResponseException $e) {
					$this->visitorService->responseError401($e->getMessage(), $e->getData());
					return $this->visitorService->HTTPresponse();
			}
	} 
}