<?php

namespace App\Modules\Visitors\Models;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
  protected $table = 'visitors';
  protected $primaryKey = 'visitor_id';
  public $timestamps = false;
}
