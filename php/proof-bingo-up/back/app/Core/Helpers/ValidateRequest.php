<?php

namespace App\Core\Helpers;

use Illuminate\Foundation\Http\FormRequest;
use App\Core\Exceptions\ResponseException;
use Illuminate\Support\Facades\Validator;

class ValidateRequest {

    public static function validate(array $request_data, array $validations){
        $validator = Validator::make($request_data, $validations);

        if ($validator->fails()) {
            $message = $validator->errors()->toArray();
            throw new ResponseException('All fileds are required*',$message);
        }
    }
    
    public static function validateObject(FormRequest $request){
      if ($request->errors){
        throw new ResponseException($request->errors->first(), $request->errors->toArray());
      }
    }
}
