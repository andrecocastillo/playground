<?php

use App\Modules\Games\Controllers\Game;
use App\Modules\Visitors\Controllers\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'visitor'], function () {
    Route::post('register-visitor', [Visitor::class, 'registerVisitor']);
    Route::get('get-board/{boardId}', [Visitor::class, 'getBoard']);
    Route::put('save-visitor-selection/{visitorId}', [Visitor::class, 'saveVisitorSelector']);
});


Route::group(['prefix' => 'game'], function () {
    Route::get('get-next-number/{gameId}', [Game::class, 'getNextNumber']);
});
