<?php

include('./models/costumer.php');
include('./models/item.php');
include('./repository/shippingapi.php');

class Store{

  private $costumer;
  private $items;

  function __construct(){
    
  }

  function setInformationCostumer($id, $firstName, $lastName, $addresses=[]){
    $Costumer = new Costumer();
    $Costumer->id = $id;
    $Costumer->firstName = $firstName;
    $Costumer->lastName = $lastName;
    $Costumer->addresses = $addresses;

    $this->costumer = $Costumer;
  }

  function appedItem($id, $name, $quantity, $price){
    $item = new Item();
    $item->id = $id;
    $item->name = $name;
    $item->quantity = $quantity;
    $item->price = $price;
    $item->priceWithQuantity();

    $this->items[] = $item;
  }

  function calculateSubtotal(){
    $value = 0;
    
    foreach($this->items as $item){
      $value += $item->priceWithQuiantity;
    }

    return $value;
  }

  function calculateTotal(){
    $shippingRate = new ShippingApi();
    $subTotal = $this->calculateSubtotal();
    $rateValue =  $shippingRate->getRate();
    return $subTotal + (($subTotal * 7) / 100) + $rateValue;
  }

  function costumer(){
    return $this->costumer;
  }

  function items(){
    return $this->items;
  }
}