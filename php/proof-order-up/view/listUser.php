<table id="users-booking" class="table table-striped">
  <tr>
    <td><b>ID</b></td>
    <td><b>TYPE</b></td>
    <td><b>FIRST NAME</b></td>
    <td><b>MIDDLE NAME</b></td>
    <td><b>LAST NAME</b></td>
    <td><b>GENDER</b></td>
    <td><b>GUEST BOOKING</b></td>
  </tr>
  <?php foreach($userData as $cl => $user){ ?>
    <tr>
      <td><?php echo $user['guest_id']; ?></td>
      <td><?php echo $user['guest_type']; ?></td>
      <td><?php echo $user['first_name']; ?></td>
      <td><?php echo $user['middle_name']; ?></td>
      <td><?php echo $user['last_name']; ?></td>
      <td><?php echo $user['gender']; ?></td>
      <td>
         <table class="booking-table">
          <tr>
            <td><b>Booking</b></td>
            <td><b>SHIP CODE</b></td>
            <td><b>NUMBER ROOM</b></td>
            <td><b>START TIME</b></td>
            <td><b>END TIME</b></td>
            <td><b>CHEKED</b></td>
          </tr>
          <?php foreach($user['guest_booking'] as $guest){ ?>
            <tr>
              <td><?php echo $guest['booking_number']; ?></td>
              <td><?php echo $guest['ship_code']; ?></td>
              <td><?php echo $guest['room_no']; ?></td>
              <td><?php echo $guest['start_time']; ?></td>
              <td><?php echo $guest['end_time']; ?></td>
              <td><?php echo $guest['is_checked_in']; ?></td>
            </tr>
          <?php } ?>
         </table>
         <br>
         <table class="booking-table">
          <tr>
            <td><b>ACCOUNT ID</b></td>
            <td><b>STATUS ID</b></td>
            <td><b>ACCOUNT LIMIT</b></td>
            <td><b>ALLOW CHARGES</b></td>
          </tr>
          <?php foreach($user['guest_account'] as $guest){ ?>
            <tr>
              <td><?php echo $guest['account_id']; ?></td>
              <td><?php echo $guest['status_id']; ?></td>
              <td><?php echo $guest['account_limit']; ?></td>
              <td><?php echo $guest['allow_charges']; ?></td>
            </tr>
          <?php } ?>
         </table>
      </td>
    </tr>
  <?php } ?>
</table>