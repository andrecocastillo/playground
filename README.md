# PLAY GROUND
This repository includes technical proofs, concept validations, updates on recent technology versions, and various investigations conducted by me.

# PROJECTS
01. Hello world react. [https://gitlab.com/andrecocastillo/playground/-/tree/main/react/hello-world]
02. Starter project. [https://gitlab.com/andrecocastillo/playground/-/tree/main/react/starter-project]
03. Todo practice. [https://gitlab.com/andrecocastillo/playground/-/tree/main/react/todo-practice]
04. Redux. [https://gitlab.com/andrecocastillo/playground/-/tree/main/react/redux]
05. php - (Tec. Proof) Order list. [https://gitlab.com/andrecocastillo/playground/-/tree/main/php/proof-order-up]
06. php - (Tec. Proof)  Bingo. [https://gitlab.com/andrecocastillo/playground/-/tree/main/php/proof-bingo-up]
07. node - (Tec. Proof)  warehouse. [https://gitlab.com/andrecocastillo/playground/-/tree/main/node/proof-wharehouse]

# NOTE
Technical proof (Tec. Proof) are projects between 4 and 8 hours. They are not a productive project. 

## License
Free

## ME
Andres Castillo
Software Developer